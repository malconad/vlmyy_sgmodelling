import ROOT
from ROOT import RooFit
import conf

def plotInTest():
    f = open("intestRes.txt")
    histo = ROOT.TH1F("","",17,7.5,92.5)
    for line in f:
        l = line.split(" ")
        hbin = histo.FindBin(int(l[3]))
        histo.SetBinContent(hbin,float(l[5]))
        histo.SetBinError(hbin,float(l[7]))

    can = ROOT.TCanvas()
    histo.SetLineColor(ROOT.kBlack)
#    histo.SetLineStyle(2)
    #histo.SetMarkerColor(conf.c_blue)
    histo.SetStats(0)
    histo.GetYaxis().SetTitle("N_{inj}/N_{fit}")
    histo.GetYaxis().SetRangeUser(0.5,1.5)
    line = ROOT.TLine(7.5,1,92.5,1);
    line.SetLineColor(conf.c_blue)
    line.SetLineWidth(2)
    line.SetLineStyle(2)
    histo.SetMarkerSize(.8)
    histo.SetMarkerStyle(20)
    histo.Draw()
    line.Draw("same")
    can.Print("sgInjection.pdf")


def estimateNsig(f):

    cutFlowEff = f.Get("cutflowEff")
    eff = cutFlowEff.GetBinContent(6)
    return 1


def sgInjection():
    mp  = conf.massPoints
    mgg = ROOT.RooRealVar("mgg", "mgg", 10, 90, "GeV") 

    nsig_fit = []
    nsig_gen = []
    ratio = []
    ratio_err = []

    #Generate asimov
    p1  = ROOT.RooRealVar   ('p1'    ,'p1'    ,5.54178, 1, 10)
    p4  = ROOT.RooRealVar   ('p4'    ,'p4'    ,-0.006, -1, 1)
    p2  = ROOT.RooRealVar   ('p2'    ,'p2'    ,1.57812, 0.1, 50)
    p3  = ROOT.RooRealVar   ('p3'    ,'p3'    ,6.67819, 0.1, 20)
    Nbkg= ROOT.RooRealVar   ('Nbkg'    ,'Nbkg'    ,180000)#check this number
    hggBkg = ROOT.RooGenericPdf("expo", "expo", "exp(mgg*p4)", ROOT.RooArgList(mgg,p4))
#    hggBkg = ROOT.RooGenericPdf("expo", "expo", "exp(-mgg*p1/1000)/(1+pow(p2,p3-mgg))", ROOT.RooArgList(mgg,p1,p2,p3))
#    component Background = HftGenericPdfBuilder("mgg", "exp(mgg*c)","c")
#    c=-0.00613636 min=-1. max 1.
    bkgPDF = ROOT.RooExtendPdf("bkgPDF","bkgPDF",hggBkg,Nbkg)
    bkgPseudoData = bkgPDF.generateBinned(ROOT.RooArgSet(mgg),RooFit.ExpectedData())
    print(bkgPseudoData.numEntries())
    print(bkgPseudoData.sumEntries())
    print(bkgPseudoData.weight())

    for index in mp:
        mggMin = int("{}".format(index-5))
        mggMax = int("{}".format(index+5))
        dcf = open("output_datacards_period_{}_ptyy{}/nom/outputParam_{}GeV.dat".format(conf.period,conf.ptyy,index))
        for line in dcf:
            if("Sigma" in line):
                sl = line.split("$")
                initVal = float(sl[3].split("\pm")[0])
                cbSigma   = ROOT.RooRealVar   ('cbSigma'  ,'cbSigma'  , initVal)
            if("AlphaHi" in line):
                sl = line.split("$")
                initVal = float(sl[3].split("\pm")[0])
                cbAlphaHi   = ROOT.RooRealVar   ('cbAlphaHi'  ,'cbAlphaHi'  , initVal)
            if("AlphaLo" in line):
                sl = line.split("$")
                initVal = float(sl[3].split("\pm")[0])
                cbAlphaLo   = ROOT.RooRealVar   ('cbAlphaLo'  ,'cbAlphaLo'  , initVal)
            if("dX" in line):
                sl = line.split("$")
                initVal = float(sl[3].split("\pm")[0])
                dX   = ROOT.RooRealVar   ('dX'  ,'dX'  , initVal)

        cbNHi     = ROOT.RooRealVar   ('cbNHi'    ,'cbNHi'    ,12)
        cbNLo     = ROOT.RooRealVar   ('cbNLo'    ,'cbNLo'    ,10)

        cbNLo.setConstant()
        cbNHi.setConstant()
        m0        = ROOT.RooFormulaVar("m0"       ,"{}+dX".format(int(index))    ,ROOT.RooArgList(dX))

#        nsig = ROOT.RooRealVar("nsig","number of signal events",0.00001,0,1) ;

        pdfSg     = ROOT.HggTwoSidedCBPdf("dscb", "dscb", mgg, m0, cbSigma,cbAlphaLo,cbNLo,cbAlphaHi,cbNHi)
#
#
        #Build Model
        nbkg = ROOT.RooRealVar("nbkg","nbkg", 179700, 0, 99999999)
        nsig = ROOT.RooRealVar( "nsig", "#signal events", 200, 0., 1000 )
        print(mgg.getVal())
#        mgg.setVal(35)
        mgg.setConstant(1)
        fitModel = ROOT.RooAddPdf( "SB", "nsig*PDFsig+nbkg*PDFbkg", ROOT.RooArgList(pdfSg,hggBkg), ROOT.RooArgList(nsig,nbkg))

        #Signal injection
        inputFile = ROOT.TFile("ntuples/nominal/mc16{}_13TeV_newMG{}_{}.root".format(conf.period,index,conf.ptyy))
        fName = "dataset_mc16{}_13TeV_newMG{}".format(conf.period,index)
        tree = inputFile.Get("tree")
        #mgg = ROOT.RooRealVar("mgg", "mgg", 0, 90, "GeV") 
#        mgg.setVal(35)
        mgg.setConstant(1)
        weight = ROOT.RooRealVar( "weight", "weight", 0.0, 1.0 ) 
#        sigData = HftData::Open(fName, "tree", fitModel, "", "weight");
        sigData = ROOT.RooDataSet(fName, fName, tree, ROOT.RooArgSet(mgg,weight), "", "weight")
        print("sigdataaa")
        print(mgg)
        sigData.Print("v")

        h_signal = ROOT.TH1D("h_signal","",9000,0,90)
        tree.Draw("mgg>>h_signal","weight*(mgg>0)")
        sigSumW = h_signal.Integral()
        sigNentries = tree.GetEntries()
        print("#####################")
        print(sigSumW)
        print(sigNentries)
        #norm = 2*20*139
        norm = 0.1*2*20*139/sigSumW

        count = 0
        toyData = bkgPseudoData.Clone("toyData")
        for n in range(sigNentries-10):#when it loads the tree it says 3 events out of range 
            evt = sigData.get(n)      
#            evt.Print("v")
#            evt = tree.GetEntry(n)            
            evnt = evt.find("mgg")
#            evnt.Print()
            #if (sigData.weight()!=0):
            peso = sigData.weight()*norm#check weight always 1
            #else:
            #    peso = norm
            toyData.add(ROOT.RooArgSet(evnt),peso)
            count = count+peso
            #toyData.Print("v")
        toyData.Print("v")

        nsig.setVal(0)
        nbkg.setVal(Nbkg.getVal())
        nsig.Print()
        nbkg.Print()
        #fitModel.fitTo(toyData)
        fitModel.fitTo(toyData,RooFit.Range(10,90))
        #fitModel.fitTo(toyData,RooFit.Verbose(1),RooFit.Range(10,90),RooFit.Strategy(2),RooFit.Offset())
        #fitModel.fitTo(toyData, "Robust:Offset:Strategy(2):Verbose(2)")
        can = ROOT.TCanvas()     
#        mggMin = int("{}".format(index-5))
#        mggMax = int("{}".format(index+5))
        fr = mgg.frame()
        #fr = mgg.frame(RooFit.Range(10,90))
        toyData.plotOn(fr)
        fitModel.plotOn(fr,RooFit.LineColor(conf.c_blue))
        fr.Draw()
        fName = "dataset_mc16{}_13TeV_newMG{}".format(conf.period,index)
        can.Print("sgInj_mc16{}_13TeV_newMG{}.pdf".format(conf.period,index))
        print(count)

        nsig_fit.append(nsig.getVal())
        nsig_gen.append(count)
        ratio_err.append(nsig.getError()/count)
        ratio.append(nsig.getVal()/count)

        print("Mass point {} GeV %%%%%%%%%%%%%%%%%%%%%%".format(index))
        print("Injected signal {}".format(count))
        print("Fit signal: {} +\- {}".format(nsig.getVal(),nsig.getError()))
        print("Ratio: {} +\- {}".format(nsig.getVal()/count,nsig.getError()/count))
        intest = open("intestRes.txt","a+")
        intest.write("Ratio for mass {} : {} +\- {}\n".format(index,nsig.getVal()/count,nsig.getError()/count))
        #massDouble[i] = mass[i];     nSigInj[i]    = estimateNsig(mass[i]);     ratio[i]      = nSigFit[i]/nSigInj[i];     ratioErr[i]   = nSigFitErr[i]/nSigFit[i];     cout<<"nSigInj = "<<nSigInj[i]<<endl;     cout<<"nSigFit = "<<nSigFit[i]<<" +/- "<<nSigFitErr[i]<<endl;     cout<<"ratio = "<<ratio[i]<<" +/- "<<ratioErr[i]<<endl;

#
#
#
