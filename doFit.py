import ROOT
from ROOT import RooFit
from conf import *
import os

print(period)
if period=="all":
    period=""

ROOT.gROOT.ProcessLine(".L HggTwoSidedCBPdf.cxx")
c_blue  = ROOT.TColor.GetColor('#3269b3')
mp = massPoints

def doExtraCheckN():
    for index in mp:
        print("Doing fit for mass point {} GeV".format(index))

        inputFile = ROOT.TFile("ntuples/nominal/mc16{}_13TeV_newMG{}_{}.root".format(period,index,ptyy))
        
        tree = inputFile.Get("tree")
        mggMin = int("{}".format(index-5))
        mggMax = int("{}".format(index+5))
        mgg = ROOT.RooRealVar("mgg", "mgg", mggMin, mggMax, "GeV") 
        weight = ROOT.RooRealVar( "weight", "weight", 0.0, 1.0 ) 
        dataSet = ROOT.RooDataSet("ds","ds",tree, ROOT.RooArgSet(mgg,weight),"","weight") 

        #Get params from single fit cards... this is not optimal since it depends on the format of the datacard
        parametrisation = open("results_period_{}_ptyy{}/paramResults/parametrisation.txt".format(period,ptyy))
        lin = parametrisation.readlines()
        sigPar = lin[0].split("=")[1]
        aHiPar = lin[1].split("=")[1]
        aLoPar = lin[2].split("=")[1]
        dmXPar = lin[3].split("=")[1]
        paramSigma = ROOT.TF1("paramSigma","{}".format(sigPar),5, 65) 
        paramAlphaHi = ROOT.TF1("paramAlphaHi","{}".format(aHiPar),5, 65) 
        paramAlphaLo = ROOT.TF1("paramAlphaLo","{}".format(aLoPar),5, 65) 
        paramdmX     = ROOT.TF1("paramdmX","{}".format(dmXPar),5, 65) 
        val_sigma    = paramSigma.Eval(index)
        val_alphahi  = paramAlphaHi.Eval(index)
        val_alphalo  = paramAlphaLo.Eval(index)
        val_dmx      = paramdmX.Eval(index)
        cbSigma      = ROOT.RooRealVar   ('cbSigma'  ,'cbSigma'  , val_sigma, val_sigma*0.9,val_sigma*1.1)
        cbAlphaHi    = ROOT.RooRealVar   ('cbAlphaHi'  ,'cbAlphaHi'  , val_alphahi, val_alphahi*0.9, val_alphahi*1.1)
        cbAlphaLo    = ROOT.RooRealVar   ('cbAlphaLo'  ,'cbAlphaLo'  , val_alphalo, val_alphalo*0.9, val_alphalo*1.1)
        dX           = ROOT.RooRealVar   ('dX'  ,'dX'  , val_dmx, val_dmx*0.9, val_dmx*1.1)

        cbNHi        = ROOT.RooRealVar   ('cbNHi'    ,'cbNHi'    ,10     ,0   ,50)#make it automatically read value
        cbNLo        = ROOT.RooRealVar   ('cbNLo'    ,'cbNLo'    ,12     ,0   ,50)

        m0        = ROOT.RooFormulaVar("m0"       ,"{}+dX".format(int(index))    ,ROOT.RooArgList(dX))

        pdfSg     = ROOT.HggTwoSidedCBPdf("dscb", "dscb", mgg, m0, cbSigma,cbAlphaLo,cbNLo,cbAlphaHi,cbNHi)
        
        nsig = ROOT.RooRealVar("nsig","number of signal events",0.00001,0,1) ;

        extPDF = ROOT.RooExtendPdf("ext","ext",pdfSg,nsig)

        #do the fit
        fitResult = pdfSg.fitTo(dataSet,RooFit.SumW2Error(ROOT.kTRUE))
        
        params = pdfSg.getParameters(dataSet)
        
        params.printLatex(RooFit.OutputFile("output_datacards_period_{}_ptyy{}/outputParam_ExtraFreeN_{}GeV.dat".format(period,ptyy,index)))


#        can = ROOT.TCanvas()
        can = ROOT.TCanvas("can", "", 30, 60, 1000, 1000)
        padUp = ROOT.TPad("padUp", "padUp", 0, 0.29, 1, 1)
        padLo = ROOT.TPad("padLo", "padLo", 0, 0, 1, 0.33)
        padUp.SetBottomMargin(0.09)
        padUp.SetBorderMode(0)
        padLo.SetTopMargin(0.01)
        padLo.SetBottomMargin(0.25)
        padLo.SetBorderMode(0)
        padUp.Draw()
        padLo.Draw()

        mggMin = float("{}".format(index-index*.25))
        mggMax = float("{}".format(index+index*.25))
        
        padUp.cd()
        fr = mgg.frame(RooFit.Range(mggMin,mggMax))
        dataSet.plotOn(fr)
        pdfSg.plotOn(fr,RooFit.LineColor(c_blue))
        chi2 = fr.chiSquare()
        fr.SetTitle("")
        fr.GetYaxis().SetTitleSize(0.05)
        fr.GetYaxis().SetTitleOffset(0.7)
        fr.Draw()
        tag = ROOT.TLatex()
        tag.SetNDC()
        tag.SetTextSize(0.05)
        tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
        tag.SetTextSize(0.03)
        tag.DrawLatex(0.15, 0.76, "#sqrt{s} = 13 TeV, X#rightarrow#gamma#gamma")
        tag.DrawLatex(0.15, 0.71, "m_{X} = "+str(index)+" GeV")
        tag.DrawLatex(0.15, 0.66, "Scalar NW single fit")
        ch = "#chi^{2}/NDF="
        tag.DrawLatex(0.15, 0.55, "{}{}".format(ch,round(chi2,2)))

        padLo.cd()
        hres = fr.residHist()
        frRes = mgg.frame(RooFit.Title("Residual Distribution"),RooFit.Range(mggMin,mggMax))
        frRes.addPlotable(hres,"P")
        frRes.SetTitle("")
        frRes.GetYaxis().SetTitle("residual")
        frRes.GetXaxis().SetTitleSize(0.1)
        frRes.GetYaxis().SetTitleSize(0.1)
        frRes.GetYaxis().SetTitleOffset(.5)
        frRes.GetYaxis().SetLabelSize(0.1)
        frRes.GetXaxis().SetTitle("m_{#gamma#gamma}[GeV]")
        frRes.GetXaxis().SetLabelSize(0.1)
        frRes.Draw()
        can.Print("results_period_{}_ptyy{}/plotsVal/massPlot_allFix_ExtraNfree_{}GeV.pdf".format(period,ptyy,index))


def doCheckN():
    for index in mp:
        print("Doing fit for mass point {} GeV".format(index))
        inputFile = ROOT.TFile("ntuples/nominal/mc16{}_13TeV_newMG{}_{}.root".format(period,index,ptyy))
        
        tree = inputFile.Get("tree")
        mggMin = int("{}".format(index-5))
        mggMax = int("{}".format(index+5))
        mgg = ROOT.RooRealVar("mgg", "mgg", mggMin, mggMax, "GeV") 
        weight = ROOT.RooRealVar( "weight", "weight", 0.0, 1.0 ) 
        dataSet = ROOT.RooDataSet("ds","ds",tree, ROOT.RooArgSet(mgg,weight),"","weight") 

        #Get params from single fit cards... this is not optimal since it depends on the format of the datacard
        parametrisation = open("results_period_{}_ptyy{}/paramResults/parametrisation.txt".format(period,ptyy))
        lin = parametrisation.readlines()
        sigPar = lin[0].split("=")[1]
        aHiPar = lin[1].split("=")[1]
        aLoPar = lin[2].split("=")[1]
        dmXPar = lin[3].split("=")[1]
        paramSigma = ROOT.TF1("paramSigma","{}".format(sigPar),5, 65) 
        paramAlphaHi = ROOT.TF1("paramAlphaHi","{}".format(aHiPar),5, 65) 
        paramAlphaLo = ROOT.TF1("paramAlphaLo","{}".format(aLoPar),5, 65) 
        paramdmX     = ROOT.TF1("paramdmX","{}".format(dmXPar),5, 65) 
        val_sigma    = paramSigma.Eval(index)
        val_alphahi  = paramAlphaHi.Eval(index)
        val_alphalo  = paramAlphaLo.Eval(index)
        val_dmx      = paramdmX.Eval(index)
        cbSigma      = ROOT.RooRealVar   ('cbSigma'  ,'cbSigma'  , val_sigma)
        cbAlphaHi    = ROOT.RooRealVar   ('cbAlphaHi'  ,'cbAlphaHi'  , val_alphahi)
        cbAlphaLo    = ROOT.RooRealVar   ('cbAlphaLo'  ,'cbAlphaLo'  , val_alphalo)
        dX           = ROOT.RooRealVar   ('dX'  ,'dX'  , val_dmx)

        #Set all params constant to the param value and free Nhi and Nlo
        cbSigma      .setConstant()
        cbAlphaHi    .setConstant()
        cbAlphaLo    .setConstant()
        dX           .setConstant()

        cbNHi        = ROOT.RooRealVar   ('cbNHi'    ,'cbNHi'    ,10     ,0   ,50)#make it automatically read vlue
        cbNLo        = ROOT.RooRealVar   ('cbNLo'    ,'cbNLo'    ,12     ,0   ,50)

        m0        = ROOT.RooFormulaVar("m0"       ,"{}+dX".format(int(index))    ,ROOT.RooArgList(dX))

        pdfSg     = ROOT.HggTwoSidedCBPdf("dscb", "dscb", mgg, m0, cbSigma,cbAlphaLo,cbNLo,cbAlphaHi,cbNHi)
        
        nsig = ROOT.RooRealVar("nsig","number of signal events",0.00001,0,1) ;

        extPDF = ROOT.RooExtendPdf("ext","ext",pdfSg,nsig)

        #do the fit
        fitResult = pdfSg.fitTo(dataSet,RooFit.SumW2Error(ROOT.kTRUE))
        
        params = pdfSg.getParameters(dataSet)
        
        params.printLatex(RooFit.OutputFile("output_datacards/outputParam_FreeN_{}GeV.dat".format(index)))


#        can = ROOT.TCanvas()
        can = ROOT.TCanvas("can", "", 30, 60, 1000, 1000)
        padUp = ROOT.TPad("padUp", "padUp", 0, 0.29, 1, 1)
        padLo = ROOT.TPad("padLo", "padLo", 0, 0, 1, 0.33)
        padUp.SetBottomMargin(0.09)
        padUp.SetBorderMode(0)
        padLo.SetTopMargin(0.01)
        padLo.SetBottomMargin(0.25)
        padLo.SetBorderMode(0)
        padUp.Draw()
        padLo.Draw()

        mggMin = float("{}".format(index-index*.25))
        mggMax = float("{}".format(index+index*.25))
        
        padUp.cd()
        fr = mgg.frame(RooFit.Range(mggMin,mggMax))
        dataSet.plotOn(fr)
        pdfSg.plotOn(fr,RooFit.LineColor(c_blue))
        chi2 = fr.chiSquare()
        fr.SetTitle("")
        fr.GetYaxis().SetTitleSize(0.05)
        fr.GetYaxis().SetTitleOffset(0.7)
        fr.Draw()
        tag = ROOT.TLatex()
        tag.SetNDC()
        tag.SetTextSize(0.05)
        tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
        tag.SetTextSize(0.03)
        tag.DrawLatex(0.15, 0.76, "#sqrt{s} = 13 TeV, X#rightarrow#gamma#gamma")
        tag.DrawLatex(0.15, 0.71, "m_{X} = "+str(index)+" GeV")
        tag.DrawLatex(0.15, 0.66, "Scalar NW single fit")
        ch = "#chi^{2}/NDF="
        tag.DrawLatex(0.15, 0.55, "{}{}".format(ch,round(chi2,2)))

        padLo.cd()
        hres = fr.residHist()
        frRes = mgg.frame(RooFit.Title("Residual Distribution"),RooFit.Range(mggMin,mggMax))
        frRes.addPlotable(hres,"P")
        frRes.SetTitle("")
        frRes.GetYaxis().SetTitle("residual")
        frRes.GetXaxis().SetTitleSize(0.1)
        frRes.GetYaxis().SetTitleSize(0.1)
        frRes.GetYaxis().SetTitleOffset(.5)
        frRes.GetYaxis().SetLabelSize(0.1)
        frRes.GetXaxis().SetTitle("m_{#gamma#gamma}[GeV]")
        frRes.GetXaxis().SetLabelSize(0.1)
        frRes.Draw()
        can.Print("results_period_{}_ptyy{}/plotsVal/massPlot_allFix_Nfree_{}GeV.pdf".format(period,ptyy,index))

def doVal():
    for index in mp:
        print("Doing fit for mass point {} GeV".format(index))
        inputFile = ROOT.TFile("ntuples/nominal/mc16{}_13TeV_newMG{}_{}.root".format(period,index,ptyy))
        
        tree = inputFile.Get("tree")
        mggMin = int("{}".format(index-5))
        mggMax = int("{}".format(index+5))
        mgg = ROOT.RooRealVar("mgg", "mgg", mggMin, mggMax, "GeV") 
        weight = ROOT.RooRealVar( "weight", "weight", 0.0, 1.0 ) 
        dataSet = ROOT.RooDataSet("ds","ds",tree, ROOT.RooArgSet(mgg,weight),"","weight") 

        #Get params from single fit cards... this is not optimal since it depends on the format of the datacard
        parametrisation = open("results_period_{}_ptyy{}/paramResults/parametrisation.txt".format(period,ptyy))
        lin = parametrisation.readlines()
        sigPar = lin[0].split("=")[1]
        aHiPar = lin[1].split("=")[1]
        aLoPar = lin[2].split("=")[1]
        dmXPar = lin[3].split("=")[1]
        paramSigma = ROOT.TF1("paramSigma","{}".format(sigPar),5, 65) 
        paramAlphaHi = ROOT.TF1("paramAlphaHi","{}".format(aHiPar),5, 65) 
        paramAlphaLo = ROOT.TF1("paramAlphaLo","{}".format(aLoPar),5, 65) 
        paramdmX = ROOT.TF1("paramdmX","{}".format(dmXPar),5, 65) 
        val_sigma   = paramSigma.Eval(index)
        val_alphahi = paramAlphaHi.Eval(index)
        val_alphalo = paramAlphaLo.Eval(index)
        val_dmx     = paramdmX.Eval(index)
        cbSigma   = ROOT.RooRealVar   ('cbSigma'  ,'cbSigma'  , val_sigma)
        cbAlphaHi   = ROOT.RooRealVar   ('cbAlphaHi'  ,'cbAlphaHi'  , val_alphahi)
        cbAlphaLo   = ROOT.RooRealVar   ('cbAlphaLo'  ,'cbAlphaLo'  , val_alphalo)
        dX   = ROOT.RooRealVar   ('dX'  ,'dX'  , val_dmx)

        cbNHi     = ROOT.RooRealVar   ('cbNHi'    ,'cbNHi'    ,10     ,0   ,100)#make it automatically read vlue
        cbNLo     = ROOT.RooRealVar   ('cbNLo'    ,'cbNLo'    ,12     ,0   ,100)

        cbNLo.setConstant()
        cbNHi.setConstant()
        m0        = ROOT.RooFormulaVar("m0"       ,"{}+dX".format(int(index))    ,ROOT.RooArgList(dX))

        pdfSg     = ROOT.HggTwoSidedCBPdf("dscb", "dscb", mgg, m0, cbSigma,cbAlphaLo,cbNLo,cbAlphaHi,cbNHi)

        can = ROOT.TCanvas("can", "", 30, 60, 1000, 1000)
#        can = ROOT.TCanvas()
        padUp = ROOT.TPad("padUp", "padUp", 0, 0.29, 1, 1)
        padLo = ROOT.TPad("padLo", "padLo", 0, 0, 1, 0.33)
        padUp.SetBottomMargin(0.09)
        padUp.SetBorderMode(0)
        padLo.SetTopMargin(0.01)
        padLo.SetBottomMargin(0.25)
        padLo.SetBorderMode(0)
        padUp.Draw()
        padLo.Draw()

        mggMin = float("{}".format(index-index*.25))
        mggMax = float("{}".format(index+index*.25))
        
        padUp.cd()
        fr = mgg.frame(RooFit.Range(mggMin,mggMax))
        dataSet.plotOn(fr)
        pdfSg.plotOn(fr,RooFit.LineColor(c_blue))
        chi2 = fr.chiSquare()
        fr.SetTitle("")
        fr.GetYaxis().SetTitleSize(0.05)
        fr.GetYaxis().SetTitleOffset(0.7)
        fr.Draw()
        tag = ROOT.TLatex()
        tag.SetNDC()
        tag.SetTextSize(0.05)
        tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
        tag.SetTextSize(0.03)
        tag.DrawLatex(0.15, 0.76, "#sqrt{s} = 13 TeV, X#rightarrow#gamma#gamma")
        tag.DrawLatex(0.15, 0.71, "m_{X} = "+str(index)+" GeV")
        tag.DrawLatex(0.15, 0.66, "Scalar NW single fit")
        ch = "#chi^{2}/NDF="
        tag.DrawLatex(0.15, 0.55, "{}{}".format(ch,round(chi2,2)))

        padLo.cd()
        hres = fr.residHist()
        frRes = mgg.frame(RooFit.Title("Residual Distribution"),RooFit.Range(mggMin,mggMax))
        frRes.addPlotable(hres,"P")
        frRes.SetTitle("")
        frRes.GetYaxis().SetTitle("residual")
        frRes.GetXaxis().SetTitleSize(0.1)
        frRes.GetYaxis().SetTitleSize(0.1)
        frRes.GetYaxis().SetTitleOffset(.5)
        frRes.GetYaxis().SetLabelSize(0.1)
        frRes.GetXaxis().SetTitle("m_{#gamma#gamma}[GeV]")
        frRes.GetXaxis().SetLabelSize(0.1)
        frRes.Draw()
        os.system("mkdir results_period_{}_ptyy{}/plotsVal".format(period,ptyy))       
        can.Print("results_period_{}_ptyy{}/plotsVal/massPlot_{}GeV.pdf".format(period,ptyy,index))


def doFirstFit(syst):
    for index in mp: 

       dc="readDC"

       if(syst!="nom"):
           inputFile = ROOT.TFile("ntuples/systematics/mc16{}_13TeV_newMG{}_ptyy_{}_{}.root".format(period,index,ptyy,syst))
       else:
           inputFile = ROOT.TFile("ntuples/nominal/mc16{}_13TeV_newMG{}_{}.root".format(period,index,ptyy))
       
       tree = inputFile.Get("tree")
       mggMin = int("{}".format(index-5))
       mggMax = int("{}".format(index+5))
       mgg = ROOT.RooRealVar("mgg", "mgg", mggMin, mggMax, "GeV") 
       weight = ROOT.RooRealVar( "weight", "weight", 0.0, 1.0 ) 
       dataSet = ROOT.RooDataSet("ds","ds",tree, ROOT.RooArgSet(mgg,weight),"","weight") 

       if(dc=="noDatacard"):
           dX        = ROOT.RooRealVar   ("dX"       ,"dX"       ,0.25   ,-3 ,3)
           cbSigma   = ROOT.RooRealVar   ('cbSigma'  ,'cbSigma'  ,0.25   ,0   ,10)
           cbAlphaHi = ROOT.RooRealVar   ('cbAlphaHi','cbAlphaHi',0.9    ,0   ,10)
           cbAlphaLo = ROOT.RooRealVar   ('cbAlphaLo','cbAlphaLo',2.5    ,0   ,10)
       #Get params from single fit cards... this is not optimal since it depends on the format of the datacard
       elif(dc=="readDC"):
           dcf = open("input_datacards/single_input_{}GeV.dat".format(index),"r")
           for line in dcf:
               if("Sigma" in line and "Hgg" not in line ):
                   sl = line.split(" ")
                   initVal = float(sl[2])
                   minVal = float(sl[3].split("=")[1])
                   maxVal = float(sl[4].split("=")[1])
                   cbSigma   = ROOT.RooRealVar   ('cbSigma'  ,'cbSigma'  , initVal   ,minVal   ,maxVal)
               if("AlphaHi" in line and "Hgg" not in line ):
                   sl = line.split(" ")
                   initVal = float(sl[2])
                   minVal = float(sl[3].split("=")[1])
                   maxVal = float(sl[4].split("=")[1])
                   cbAlphaHi   = ROOT.RooRealVar   ('cbAlphaHi'  ,'cbAlphaHi'  , initVal   ,minVal   ,maxVal)
               if("AlphaLo" in line and "Hgg" not in line ):
                   sl = line.split(" ")
                   initVal = float(sl[2])
                   minVal = float(sl[3].split("=")[1])
                   maxVal = float(sl[4].split("=")[1])
                   cbAlphaLo   = ROOT.RooRealVar   ('cbAlphaLo'  ,'cbAlphaLo'  , initVal   ,minVal   ,maxVal)
               if("dmX" in line and "cbPeak" not in line ):
                   sl = line.split(" ")
                   initVal = float(sl[2])
                   minVal = float(sl[3].split("=")[1])
                   maxVal = float(sl[4].split("=")[1])
                   dX   = ROOT.RooRealVar   ('dX'  ,'dX'  , initVal   ,minVal   ,maxVal)
               if("cbNLo" in line and "cbPeak" not in line ):
                   sl = line.split(" ")
                   initVal = float(sl[2])
#                   minVal = float(sl[3].split("=")[1])
#                   maxVal = float(sl[4].split("=")[1])
                   cbNLo   = ROOT.RooRealVar   ('cbNLo'  ,'cbNLo'  , initVal   ,0   ,50)
               if("cbNHi" in line and "cbPeak" not in line ):
                   sl = line.split(" ")
                   initVal = float(sl[2])
#                   minVal = float(sl[3].split("=")[1])
#                   maxVal = float(sl[4].split("=")[1])
                   cbNHi   = ROOT.RooRealVar   ('cbNHi'  ,'cbNHi'  , initVal   ,0   ,100)

       cbNLo.setConstant()
       cbNHi.setConstant()
       m0        = ROOT.RooFormulaVar("m0"       ,"{}+dX".format(int(index))    ,ROOT.RooArgList(dX))

       nsig = ROOT.RooRealVar("nsig","number of signal events",0.00001,0,1) ;

       pdfSg     = ROOT.HggTwoSidedCBPdf("dscb", "dscb", mgg, m0, cbSigma,cbAlphaLo,cbNLo,cbAlphaHi,cbNHi)

       extPDF = ROOT.RooExtendPdf("ext","ext",pdfSg,nsig)

       #do the fit
       #check the bottom of the document for fitting options
       fitResult = pdfSg.fitTo(dataSet,RooFit.SumW2Error(ROOT.kTRUE))
       #fitResult = pdfSg.fitTo(dataSet,RooFit.SumW2Error(ROOT.kTRUE),RooFit.Verbose(1))


       params = pdfSg.getParameters(dataSet)
       if(os.path.isdir("output_datacards_period_{}_ptyy{}".format(period,ptyy))==False):
           os.system("mkdir output_datacards_period_{}_ptyy{}".format(period,ptyy))       
       if(os.path.isdir("output_datacards_period_{}_ptyy{}/{}".format(period,ptyy,syst,index))==False):
           os.system("mkdir output_datacards_period_{}_ptyy{}/{}".format(period,ptyy,syst))       
       params.printLatex(RooFit.OutputFile("output_datacards_period_{}_ptyy{}/{}/outputParam_{}GeV.dat".format(period,ptyy,syst,index)))

       #can = ROOT.TCanvas()
       can = ROOT.TCanvas("can", "", 30, 60, 1000, 1000)
       padUp = ROOT.TPad("padUp", "padUp", 0, 0.29, 1, 1)
       padLo = ROOT.TPad("padLo", "padLo", 0, 0, 1, 0.33)
       padUp.SetBottomMargin(0.09)
       padUp.SetBorderMode(0)
       padLo.SetTopMargin(0.01)
       padLo.SetBottomMargin(0.25)
       padLo.SetBorderMode(0)
       padUp.Draw()
       padLo.Draw()

       mggMin = float("{}".format(index-index*.25))
       mggMax = float("{}".format(index+index*.25))
       
       padUp.cd()
       fr = mgg.frame(RooFit.Range(mggMin,mggMax))
       dataSet.plotOn(fr)
       pdfSg.plotOn(fr,RooFit.LineColor(c_blue))
       chi2 = fr.chiSquare()
       fr.SetTitle("")
       fr.GetYaxis().SetTitleSize(0.05)
       fr.GetYaxis().SetTitleOffset(0.7)
       fr.Draw()
       tag = ROOT.TLatex()
       tag.SetNDC()
       tag.SetTextSize(0.05)
       tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
       tag.SetTextSize(0.03)
       tag.DrawLatex(0.15, 0.76, "#sqrt{s} = 13 TeV, X#rightarrow#gamma#gamma")
       tag.DrawLatex(0.15, 0.71, "m_{X} = "+str(index)+" GeV")
       tag.DrawLatex(0.15, 0.66, "Scalar NW single fit")
       ch = "#chi^{2}/NDF="
       tag.DrawLatex(0.15, 0.60, "{}{}".format(ch,round(chi2,2)))

       padLo.cd()
       hres = fr.residHist()
       frRes = mgg.frame(RooFit.Title("Residual Distribution"),RooFit.Range(mggMin,mggMax))
       frRes.addPlotable(hres,"P")
       frRes.SetTitle("")
       frRes.GetYaxis().SetTitle("residual")
       frRes.GetXaxis().SetTitleSize(0.1)
       frRes.GetYaxis().SetTitleSize(0.1)
       frRes.GetYaxis().SetTitleOffset(.5)
       frRes.GetYaxis().SetLabelSize(0.1)
       frRes.GetXaxis().SetTitle("m_{#gamma#gamma}[GeV]")
       frRes.GetXaxis().SetLabelSize(0.1)
       frRes.Draw()
       if(os.path.isdir("results_period_{}_ptyy{}".format(period,ptyy))==False):
           os.system("mkdir results_period_{}_ptyy{}".format(period,ptyy))       
       if(os.path.isdir("results_period_{}_ptyy{}/{}".format(period,ptyy,syst))==False):
           os.system("mkdir results_period_{}_ptyy{}/{}".format(period,ptyy,syst))       
       if(os.path.isdir("results_period_{}_ptyy{}/{}/plots".format(period,ptyy,syst))==False):
           os.system("mkdir results_period_{}_ptyy{}/{}/plots".format(period,ptyy,syst))       
       can.Print("results_period_{}_ptyy{}/{}/plots/massPlot_{}GeV.pdf".format(period,ptyy,syst,index))



#
#
                #fit = extPDF.fitTo(dataSet,RooFit.SumW2Error(ROOT.kTRUE))
#                fit = pdfSg.fitTo(dataSet,RooFit.SumW2Error(ROOT.kTRUE))
#                fit = pdfSg.fitTo(dataSet,RooFit.Range(9.5,12),RooFit.SumW2Error(ROOT.kTRUE))
#                fitResult = extPDF.fitTo(dataSet,RooFit.SumW2Error(ROOT.kTRUE), RooFit.Save(True), RooFit.Minos(True))
                #fitResult = pdfSg.fitTo(dataSet,RooFit.SumW2Error(ROOT.kTRUE), RooFit.Minos(True))
                #fitResult = pdfSg.fitTo(dataSet, RooFit.Save(True), RooFit.Minos(True), RooFit.PrintLevel(-1))

#def doSystRes(shift):
#    for index in mp: 
#
#       inputFile = ROOT.TFile("ntuples/systematics/mc16{}_13TeV_newMG{}_ptyy_{}_{}.root".format(period,index,ptyy,shift))
#       
#       tree = inputFile.Get("tree")
#       mggMin = int("{}".format(index-5))
#       mggMax = int("{}".format(index+5))
#       mgg = ROOT.RooRealVar("mgg", "mgg", mggMin, mggMax, "GeV") 
#       weight = ROOT.RooRealVar( "weight", "weight", 0.0, 1.0 ) 
#       dataSet = ROOT.RooDataSet("ds","ds",tree, ROOT.RooArgSet(mgg,weight),"","weight") 
#       #Get params from single fit cards... this is not optimal since it depends on the format of the datacard
#       elif(dc=="readDC"):
#           dcf = open("input_datacards/single_input_{}GeV.dat".format(index),"r")
#           for line in dcf:
#               if("Sigma" in line and "Hgg" not in line ):
#                   sl = line.split(" ")
#                   initVal = float(sl[2])
#                   minVal = float(sl[3].split("=")[1])
#                   maxVal = float(sl[4].split("=")[1])
#                   cbSigma   = ROOT.RooRealVar   ('cbSigma'  ,'cbSigma'  , initVal   ,minVal   ,maxVal)
#               if("AlphaHi" in line and "Hgg" not in line ):
#                   sl = line.split(" ")
#                   initVal = float(sl[2])
#                   minVal = float(sl[3].split("=")[1])
#                   maxVal = float(sl[4].split("=")[1])
#                   cbAlphaHi   = ROOT.RooRealVar   ('cbAlphaHi'  ,'cbAlphaHi'  , initVal   ,minVal   ,maxVal)
#               if("AlphaLo" in line and "Hgg" not in line ):
#                   sl = line.split(" ")
#                   initVal = float(sl[2])
#                   minVal = float(sl[3].split("=")[1])
#                   maxVal = float(sl[4].split("=")[1])
#                   cbAlphaLo   = ROOT.RooRealVar   ('cbAlphaLo'  ,'cbAlphaLo'  , initVal   ,minVal   ,maxVal)
#               if("dmX" in line and "cbPeak" not in line ):
#                   sl = line.split(" ")
#                   initVal = float(sl[2])
#                   minVal = float(sl[3].split("=")[1])
#                   maxVal = float(sl[4].split("=")[1])
#                   dX   = ROOT.RooRealVar   ('dX'  ,'dX'  , initVal   ,minVal   ,maxVal)
#               if("cbNLo" in line and "cbPeak" not in line ):
#                   sl = line.split(" ")
#                   initVal = float(sl[2])
##                   minVal = float(sl[3].split("=")[1])
##                   maxVal = float(sl[4].split("=")[1])
#                   cbNLo   = ROOT.RooRealVar   ('cbNLo'  ,'cbNLo'  , initVal   ,0   ,50)
#               if("cbNHi" in line and "cbPeak" not in line ):
#                   sl = line.split(" ")
#                   initVal = float(sl[2])
##                   minVal = float(sl[3].split("=")[1])
##                   maxVal = float(sl[4].split("=")[1])
#                   cbNHi   = ROOT.RooRealVar   ('cbNHi'  ,'cbNHi'  , initVal   ,0   ,100)
#
#       cbNLo.setConstant()
#       cbNHi.setConstant()
#       m0        = ROOT.RooFormulaVar("m0"       ,"{}+dX".format(int(index))    ,ROOT.RooArgList(dX))
#
#       nsig = ROOT.RooRealVar("nsig","number of signal events",0.00001,0,1) ;
#
#       pdfSg     = ROOT.HggTwoSidedCBPdf("dscb", "dscb", mgg, m0, cbSigma,cbAlphaLo,cbNLo,cbAlphaHi,cbNHi)
#
#       extPDF = ROOT.RooExtendPdf("ext","ext",pdfSg,nsig)
#
#       #do the fit
#       #check the bottom of the document for fitting options
#       fitResult = pdfSg.fitTo(dataSet,RooFit.SumW2Error(ROOT.kTRUE))
#
#
#       params = pdfSg.getParameters(dataSet)
#       os.system("mkdir output_datacards_period_{}_ptyy{}".format(period,ptyy))       
#       params.printLatex(RooFit.OutputFile("output_datacards_period_{}_ptyy{}/outputParam_{}GeV.dat".format(period,ptyy,index)))
#
#       #can = ROOT.TCanvas()
#       can = ROOT.TCanvas("can", "", 30, 60, 1000, 1000)
#       padUp = ROOT.TPad("padUp", "padUp", 0, 0.29, 1, 1)
#       padLo = ROOT.TPad("padLo", "padLo", 0, 0, 1, 0.33)
#       padUp.SetBottomMargin(0.09)
#       padUp.SetBorderMode(0)
#       padLo.SetTopMargin(0.01)
#       padLo.SetBottomMargin(0.25)
#       padLo.SetBorderMode(0)
#       padUp.Draw()
#       padLo.Draw()
#
#       mggMin = float("{}".format(index-index*.25))
#       mggMax = float("{}".format(index+index*.25))
#       
#       padUp.cd()
#       fr = mgg.frame(RooFit.Range(mggMin,mggMax))
#       dataSet.plotOn(fr)
#       pdfSg.plotOn(fr,RooFit.LineColor(c_blue))
#       chi2 = fr.chiSquare()
#       fr.SetTitle("")
#       fr.GetYaxis().SetTitleSize(0.05)
#       fr.GetYaxis().SetTitleOffset(0.7)
#       fr.Draw()
#       tag = ROOT.TLatex()
#       tag.SetNDC()
#       tag.SetTextSize(0.05)
#       tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
#       tag.SetTextSize(0.03)
#       tag.DrawLatex(0.15, 0.76, "#sqrt{s} = 13 TeV, X#rightarrow#gamma#gamma")
#       tag.DrawLatex(0.15, 0.71, "m_{X} = "+str(index)+" GeV")
#       tag.DrawLatex(0.15, 0.66, "Scalar NW single fit")
#       ch = "#chi^{2}/NDF="
#       tag.DrawLatex(0.15, 0.60, "{}{}".format(ch,round(chi2,2)))
#
#       padLo.cd()
#       hres = fr.residHist()
#       frRes = mgg.frame(RooFit.Title("Residual Distribution"),RooFit.Range(mggMin,mggMax))
#       frRes.addPlotable(hres,"P")
#       frRes.SetTitle("")
#       frRes.GetYaxis().SetTitle("residual")
#       frRes.GetXaxis().SetTitleSize(0.1)
#       frRes.GetYaxis().SetTitleSize(0.1)
#       frRes.GetYaxis().SetTitleOffset(.5)
#       frRes.GetYaxis().SetLabelSize(0.1)
#       frRes.GetXaxis().SetTitle("m_{#gamma#gamma}[GeV]")
#       frRes.GetXaxis().SetLabelSize(0.1)
#       frRes.Draw()
#       os.system("mkdir results_period_{}_ptyy{}".format(period,ptyy))       
#       os.system("mkdir results_period_{}_ptyy{}/plots".format(period,ptyy))       
#       can.Print("results_period_{}_ptyy{}/plots/massPlot_{}GeV.pdf".format(period,ptyy,index))
#
