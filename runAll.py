import ROOT
from ROOT import RooFit
import plotSigParam
import doFit
import plotEffs
import sgInjection

#To choose the mass points, periods and some other configurations please edit conf.py
##Do you want to do the single fits?
#doFit.doFirstFit("nom")

#Do you want to plot the parameters of the fit and get a table with the parametrisation?
#plotSigParam.plotParam()
#plotSigParam.compParam()
#####First check
#####Plot the MC with the PDFs obtained using the linearlly parametrised values of its parameters?
#doFit.doVal()
#######Second check
#######Fix DSCB params to parametrisation values and free N
####Este check no tiene mucho sentido... el siguiente s[i
###doFit.doCheckN()
###plotSigParam.plotParam_freeN()
#######Third check
#######Fix DSCB params to parametrisation values and let them move in \pm 1%, and free N
#doFit.doExtraCheckN()
#plotSigParam.plotParam_ExtrafreeN()
####
####### For plotting the signal selection efficiency for the different periods you can use the following function (I'm not proud of this script, it's not very smart but it works)
#plotEffs.plotCFEffs()
##Signal injection
#sgInjection.sgInjection()
#sgInjection.plotInTest()
#Systematics
#doFit.doFirstFit("resolUp")
#doFit.doFirstFit("resolDown")
#doFit.doFirstFit("scaleUp")
#doFit.doFirstFit("scaleDown")
plotSigParam.plotParam_syst()
#doFit.doSystRes("resolUp")
