import ROOT
import conf
import os
from conf import *

def prettyHist(histo):
    histo.SetStats(0)
    histo.SetMarkerSize(0.8)
    histo.SetMarkerStyle(20) 
    histo.SetLineWidth(2) 
    histo.SetTitle("")
    histo.GetXaxis().SetTitle("m_{X} [GeV]")
    histo.GetYaxis().SetTitle("Total selection efficiency")

def plotCFEffs():
    trig_a = []
    pres_a = []
    ID_a   = []
    iso_a  = []
    ptyy_a = []
    trig_d = []
    pres_d = []
    ID_d   = []
    iso_d  = []
    ptyy_d = []
    trig_e = []
    pres_e = []
    ID_e   = []
    iso_e  = []
    ptyy_d = []
    ptyy_e = []
    period = ["a","d","e"]
    nbins = int((massPoints[len(massPoints)-1]+2.5-massPoints[0]+2.5)/5)
    for mp in massPoints:
        inputF = ROOT.TFile("ntuples/nominal/mc16a_13TeV_newMG{}_{}.root".format(mp,conf.ptyy))
        cutFEff = inputF.Get("cutflowEff")
        trig_a.append(cutFEff.GetBinContent(2))
        pres_a.append(cutFEff.GetBinContent(3))
        ID_a.append(cutFEff.GetBinContent(4))
        iso_a.append(cutFEff.GetBinContent(5))
        ptyy_a.append(cutFEff.GetBinContent(6))
    for mp in massPoints:
        inputF = ROOT.TFile("ntuples/nominal/mc16d_13TeV_newMG{}_{}.root".format(mp,conf.ptyy))
        cutFEff = inputF.Get("cutflowEff")
        trig_d.append(cutFEff.GetBinContent(2))
        pres_d.append(cutFEff.GetBinContent(3))
        ID_d.append(cutFEff.GetBinContent(4))
        iso_d.append(cutFEff.GetBinContent(5))
        ptyy_d.append(cutFEff.GetBinContent(6))
    for mp in massPoints:
        inputF = ROOT.TFile("ntuples/nominal/mc16e_13TeV_newMG{}_{}.root".format(mp,conf.ptyy))
        cutFEff = inputF.Get("cutflowEff")
        trig_e.append(cutFEff.GetBinContent(2))
        pres_e.append(cutFEff.GetBinContent(3))
        ID_e.append(cutFEff.GetBinContent(4))
        iso_e.append(cutFEff.GetBinContent(5))
        ptyy_e.append(cutFEff.GetBinContent(6))


    h_CFtrig_mc16a_eff   = ROOT.TH1F("CFtrig_mc16a_eff"  ,"CFtrig_mc16a_eff"  ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFpres_mc16a_eff   = ROOT.TH1F("CFpres_mc16a_eff"  ,"CFpres_mc16a_eff"  ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFID_mc16a_eff     = ROOT.TH1F("CFID_mc16a_eff"    ,"CFID_mc16a_eff"    ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFiso_mc16a_eff    = ROOT.TH1F("CFiso_mc16a_eff"   ,"CFiso_mc16a_eff"   ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFptyy_mc16a_eff   = ROOT.TH1F("CFptyy_mc16a_eff"  ,"CFptyy_mc16a_eff"  ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFtrig_mc16d_eff   = ROOT.TH1F("CFtrig_mc16d_eff"  ,"CFtrig_mc16d_eff"  ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFpres_mc16d_eff   = ROOT.TH1F("CFpres_mc16d_eff"  ,"CFpres_mc16d_eff"  ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFID_mc16d_eff     = ROOT.TH1F("CFID_mc16d_eff"    ,"CFID_mc16d_eff"    ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFiso_mc16d_eff    = ROOT.TH1F("CFiso_mc16d_eff"   ,"CFiso_mc16d_eff"   ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFptyy_mc16d_eff   = ROOT.TH1F("CFptyy_mc16d_eff"  ,"CFptyy_mc16d_eff"  ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFtrig_mc16e_eff   = ROOT.TH1F("CFtrig_mc16e_eff"  ,"CFtrig_mc16e_eff"  ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFpres_mc16e_eff   = ROOT.TH1F("CFpres_mc16e_eff"  ,"CFpres_mc16e_eff"  ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFID_mc16e_eff     = ROOT.TH1F("CFID_mc16e_eff"    ,"CFID_mc16e_eff"    ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFiso_mc16e_eff    = ROOT.TH1F("CFiso_mc16e_eff"   ,"CFiso_mc16e_eff"   ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    h_CFptyy_mc16e_eff   = ROOT.TH1F("CFptyy_mc16e_eff"  ,"CFptyy_mc16e_eff"  ,nbins,massPoints[0]-2.5,massPoints[len(massPoints)-1]+2.5)
    
    for i in range(len(trig_a)):
        iBin = h_CFtrig_mc16a_eff.FindBin(massPoints[i])
        h_CFtrig_mc16a_eff    .SetBinContent(iBin,float(trig_a[i]))
        h_CFpres_mc16a_eff    .SetBinContent(iBin,float(pres_a[i]))
        h_CFID_mc16a_eff      .SetBinContent(iBin,float(ID_a[i]))
        h_CFiso_mc16a_eff     .SetBinContent(iBin,float(iso_a[i]))
        h_CFptyy_mc16a_eff    .SetBinContent(iBin,float(ptyy_a[i]))
        h_CFtrig_mc16d_eff    .SetBinContent(iBin,float(trig_d[i]))
        h_CFpres_mc16d_eff    .SetBinContent(iBin,float(pres_d[i]))
        h_CFID_mc16d_eff      .SetBinContent(iBin,float(ID_d[i]))
        h_CFiso_mc16d_eff     .SetBinContent(iBin,float(iso_d[i]))
        h_CFptyy_mc16d_eff    .SetBinContent(iBin,float(ptyy_d[i]))
        h_CFtrig_mc16e_eff    .SetBinContent(iBin,float(trig_e[i]))
        h_CFpres_mc16e_eff    .SetBinContent(iBin,float(pres_e[i]))
        h_CFID_mc16e_eff      .SetBinContent(iBin,float(ID_e[i]))
        h_CFiso_mc16e_eff     .SetBinContent(iBin,float(iso_e[i]))
        h_CFptyy_mc16e_eff    .SetBinContent(iBin,float(ptyy_e[i]))
    
    os.system("mkdir Effs".format(period,ptyy))       
    can = ROOT.TCanvas()
    leg=ROOT.TLegend(.68,.18,.88,.34)
    leg.SetBorderSize(0)
    prettyHist(h_CFtrig_mc16a_eff)
    prettyHist(h_CFpres_mc16a_eff)
    prettyHist(h_CFID_mc16a_eff)
    prettyHist(h_CFiso_mc16a_eff)
    prettyHist(h_CFptyy_mc16a_eff)
    h_CFtrig_mc16a_eff.SetLineColor(c_blue)
    h_CFpres_mc16a_eff.SetLineColor(c_lblue)
    h_CFID_mc16a_eff.SetLineColor(c_yellow)
    h_CFiso_mc16a_eff.SetLineColor(c_green)
    h_CFptyy_mc16a_eff.SetLineColor(c_red)
    prettyHist(h_CFtrig_mc16d_eff)
    prettyHist(h_CFpres_mc16d_eff)
    prettyHist(h_CFID_mc16d_eff)
    prettyHist(h_CFiso_mc16d_eff)
    prettyHist(h_CFptyy_mc16d_eff)
    h_CFtrig_mc16d_eff.SetLineColor(c_blue)
    h_CFpres_mc16d_eff.SetLineColor(c_lblue)
    h_CFID_mc16d_eff.SetLineColor(c_yellow)
    h_CFiso_mc16d_eff.SetLineColor(c_green)
    h_CFptyy_mc16d_eff.SetLineColor(c_red)
    prettyHist(h_CFtrig_mc16e_eff)
    prettyHist(h_CFpres_mc16e_eff)
    prettyHist(h_CFID_mc16e_eff)
    prettyHist(h_CFiso_mc16e_eff)
    prettyHist(h_CFptyy_mc16e_eff)
    h_CFtrig_mc16e_eff.SetLineColor(c_blue)
    h_CFpres_mc16e_eff.SetLineColor(c_lblue)
    h_CFID_mc16e_eff.SetLineColor(c_yellow)
    h_CFiso_mc16e_eff.SetLineColor(c_green)
    h_CFptyy_mc16e_eff.SetLineColor(c_red)



    leg.AddEntry(h_CFtrig_mc16a_eff,"Trigger","L")
    #leg.AddEntry(h_CFpres_mc16a_eff,"+p_{T}(#gamma)>22 GeV","L")
    leg.AddEntry(h_CFID_mc16a_eff,"+ID","L")
    leg.AddEntry(h_CFiso_mc16a_eff,"+Isolation","L")
    leg.AddEntry(h_CFptyy_mc16a_eff,"+p_{T}(#gamma#gamma)>50 GeV","L")


    h_CFtrig_mc16a_eff.Draw("")
    #h_CFpres_mc16a_eff.Draw("same")
    h_CFID_mc16a_eff.Draw("same")
    h_CFiso_mc16a_eff.Draw("same")
    h_CFptyy_mc16a_eff.Draw("same")
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
#    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    tag.DrawLatex(0.15, 0.77, "#bf{MC16a}")
    leg.Draw()
    can.Print("Effs/CFeff_mc16a_ptyy{}.pdf".format(ptyy))
    h_CFtrig_mc16d_eff.Draw("")
    #h_CFpres_mc16d_eff.Draw("same")
    h_CFID_mc16d_eff.Draw("same")
    h_CFiso_mc16d_eff.Draw("same")
    h_CFptyy_mc16d_eff.Draw("same")
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
 #   tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    tag.DrawLatex(0.15, 0.77, "#bf{MC16d}")
    leg.Draw()
    can.Print("Effs/CFeff_mc16d_ptyy{}.pdf".format(ptyy))
    h_CFtrig_mc16e_eff.Draw("")
    #h_CFpres_mc16e_eff.Draw("same")
    h_CFID_mc16e_eff.Draw("same")
    h_CFiso_mc16e_eff.Draw("same")
    h_CFptyy_mc16e_eff.Draw("same")
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
#    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    tag.DrawLatex(0.15, 0.77, "#bf{MC16e}")
    leg.Draw()
    can.Print("Effs/CFeff_mc16e_ptyy{}.pdf".format(ptyy))
