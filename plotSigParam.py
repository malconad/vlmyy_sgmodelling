import ROOT
import os
from conf import *

mp = massPoints

def plotParam():
    cbSigma     = []
    cbSigmaErr     = []
    cbAlphaHi     = []
    cbAlphaHiErr = []
    cbAlphaLo   = []
    cbAlphaLoErr = []
    dmX         = []
    dmXErr         = []
    outputList = []
    for i in mp:
        fileName = "output_datacards_period_{}_ptyy{}/nom/outputParam_{}GeV.dat".format(period,ptyy,i) 
        outputList.append(fileName)
    
    for f in outputList:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "dX" in line:
                            dmX.append(l[5].split("\\")[0])
                            dmXErr.append(l[6].split("$")[0])
                        if "cbSigma" in line:
                            cbSigma.append(l[5].split("\\")[0])
                            cbSigmaErr.append(l[6].split("$")[0])
                        if "cbAlphaLo" in line:
                            cbAlphaLo.append(l[5].split("\\")[0])
                            cbAlphaLoErr.append(l[6].split("$")[0])
                        if "cbAlphaHi" in line:
                            cbAlphaHi.append(l[5].split("\\")[0])
                            cbAlphaHiErr.append(l[6].split("$")[0])
    
    nbins = int((mp[len(mp)-1]+2.5-mp[0]+2.5)/5)
#    print("number of bins {}".format(nbins))
#    print("mp[0]-2.5 {}".format(mp[0]-2.5))
#    print("mp[len(mp)-1]+2.5 {}".format(mp[len(mp)-1]+2.5))
    hist_cbSigma   = ROOT.TH1F("cbSigma"  ,"cbSigma"  ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_cbAlphaHi = ROOT.TH1F("cbAlphaHi","cbAlphaHi",nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_cbAlphaLo = ROOT.TH1F("cbAlphaLo","cbAlphaLo",nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_dmX       = ROOT.TH1F("dmX"      ,"dmX"      ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_nHi       = ROOT.TH1F("nHi"      ,"nHi"      ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_nLo       = ROOT.TH1F("nLo"      ,"nLo"      ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    
    for i in range(len(cbSigma)):
        iBin = hist_cbSigma.FindBin(mp[i])
#        print("mass points {}".format(mp))
#        print("mp[i] {}".format(mp[i]))
#        print("iBin {}".format(iBin))
#        print("bin centre {}".format(hist_cbSigma.GetBinCenter(iBin)))

        hist_cbSigma    .SetBinContent(iBin,float(cbSigma[i]))
        hist_cbSigma    .SetBinError  (iBin,float(cbSigmaErr[i]))
        hist_cbAlphaHi  .SetBinContent(iBin,float(cbAlphaHi[i]))
        hist_cbAlphaHi  .SetBinError  (iBin,float(cbAlphaHiErr[i]))
        hist_cbAlphaLo  .SetBinContent(iBin,float(cbAlphaLo[i]))
        hist_cbAlphaLo  .SetBinError  (iBin,float(cbAlphaLoErr[i]))
        hist_dmX        .SetBinContent(iBin,float(dmX[i]))
        hist_dmX        .SetBinError  (iBin,float(dmXErr[i]))
        hist_nHi        .SetBinContent(iBin,float(10))
        hist_nLo        .SetBinContent(iBin,float(12))
    
    listOfHistos = [hist_cbSigma,hist_cbAlphaHi,hist_cbAlphaLo,hist_dmX,hist_nHi,hist_nLo]
    for histo in listOfHistos:
        if(histo==hist_cbSigma):
            histo.GetYaxis().SetTitle("#sigma_{CB} [GeV]")
            histo.GetYaxis().SetRangeUser(0,1.8)
        if(histo==hist_cbAlphaHi):
            histo.GetYaxis().SetTitle("#alpha_{High}")
            histo.GetYaxis().SetRangeUser(.5,2.5)
        if(histo==hist_cbAlphaLo):
            histo.GetYaxis().SetTitle("#alpha_{Low}")
            histo.GetYaxis().SetRangeUser(.5,2.5)
        if(histo==hist_dmX):
            histo.GetYaxis().SetTitle("#Delta m_{X}")
            histo.GetYaxis().SetRangeUser(0,0.1)
        if(histo==hist_nHi):
            histo.GetYaxis().SetTitle("N_{High}")
            histo.GetYaxis().SetRangeUser(5,15)
        if(histo==hist_nLo):
            histo.GetYaxis().SetTitle("N_{Low}")
            histo.GetYaxis().SetRangeUser(7,17)

        histo.SetStats(0)
        histo.SetMarkerSize(0.8)
        histo.SetMarkerStyle(20) 
        histo.SetLineWidth(2) 
        histo.SetLineColor(ROOT.kBlack) 
        histo.SetMarkerColor(ROOT.kBlack) 
        histo.SetTitle("")
        ROOT.gStyle.SetErrorX(0)
        ROOT.gStyle.SetOptFit(1111)
        histo.GetXaxis().SetTitle("m_{X} [GeV]")
    
    os.system("mkdir results_period_{}_ptyy{}/paramResults".format(period,ptyy))       
    saveParam = open("results_period_{}_ptyy{}/paramResults/parametrisation.txt".format(period,ptyy), "w")

    c_red   = ROOT.TColor.GetColor('#b33c32')
    c_blue  = ROOT.TColor.GetColor('#3269b3')
    
    can = ROOT.TCanvas()
    leg=ROOT.TLegend(.68,.74,.88,.84)
    leg.SetBorderSize(0)
    hist_cbSigma.Fit("pol1")
    hist_cbSigma.Draw()
    leg.AddEntry(hist_cbSigma,"Single Fit","P")
    leg.AddEntry(hist_cbSigma.GetFunction("pol1"),"Parametrisation","L")
    leg.Draw()
    hist_cbSigma.GetFunction("pol1").SetLineColor(c_blue)
    hist_cbSigma.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    offse_sigma = hist_cbSigma.GetFunction("pol1").GetParameter(0)
    slope_sigma = hist_cbSigma.GetFunction("pol1").GetParameter(1)
    saveParam.write("paramSigma = {}*x+{}\n".format(slope_sigma,offse_sigma))
    can.Print("results_period_{}_ptyy{}/paramResults/sigma.pdf".format(period,ptyy))

    hist_cbAlphaHi.Fit("pol1")
    hist_cbAlphaHi.Draw()
    leg.Draw()
    hist_cbAlphaHi.GetFunction("pol1").SetLineColor(c_blue)
    hist_cbAlphaHi.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    offse_alphaHi = hist_cbAlphaHi.GetFunction("pol1").GetParameter(0)
    slope_alphaHi = hist_cbAlphaHi.GetFunction("pol1").GetParameter(1)
    saveParam.write("paramAlphaHi = {}*x+{}\n".format(slope_alphaHi,offse_alphaHi))
    can.Print("results_period_{}_ptyy{}/paramResults/alphaHi.pdf".format(period,ptyy))

    hist_cbAlphaLo.Fit("pol1")
    hist_cbAlphaLo.Draw()
    leg.Draw()
    hist_cbAlphaLo.GetFunction("pol1").SetLineColor(c_blue)
    hist_cbAlphaLo.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    offse_alphaLo = hist_cbAlphaLo.GetFunction("pol1").GetParameter(0)
    slope_alphaLo = hist_cbAlphaLo.GetFunction("pol1").GetParameter(1)
    saveParam.write("paramAlphaLow = {}*x+{}\n".format(slope_alphaLo,offse_alphaLo))
    can.Print("results_period_{}_ptyy{}/paramResults/alphaLo.pdf".format(period,ptyy))


    hist_dmX.Fit("pol1")
    hist_dmX.Draw()
    leg.Draw()
    hist_dmX.GetFunction("pol1").SetLineColor(c_blue)
    hist_dmX.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    offse_dmX = hist_dmX.GetFunction("pol1").GetParameter(0)
    slope_dmX = hist_dmX.GetFunction("pol1").GetParameter(1)
    saveParam.write("paramdmX = {}*x+{}\n".format(slope_dmX,offse_dmX))
    can.Print("results_period_{}_ptyy{}/paramResults/dmx.pdf".format(period,ptyy))

    hist_nHi.Fit("pol1")
    hist_nHi.Draw("P")
    leg.Draw()
    hist_nHi.GetFunction("pol1").SetLineColor(c_blue)
    hist_nHi.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    can.Print("results_period_{}_ptyy{}/paramResults/nHi.pdf".format(period,ptyy))

    hist_nLo.Fit("pol1")
    hist_nLo.Draw("P")
    leg.Draw()
    hist_nLo.GetFunction("pol1").SetLineColor(c_blue)
    hist_nLo.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    can.Print("results_period_{}_ptyy{}/paramResults/nLo.pdf".format(period,ptyy))

    saveParam = open("results_period_{}_ptyy{}/paramResults/parametrisation.tex".format(period,ptyy), "w")
    saveParam.write("\\documentclass{article}\n")
    saveParam.write("\\pagestyle{empty}\n")
    saveParam.write("\\begin{document}\n")
    saveParam.write("\\begin{tabular}{l | l}\n")
    saveParam.write("\\hline\n")
    saveParam.write("\\hline\n")
    saveParam.write("DSCB parameter  & Mass parametrisation\\\\\\hline\n")
    saveParam.write("$\sigma_{CB}$     &"+"{}".format(round(slope_sigma,3))  +"+$m_{X}$" +    "{}\\\\\n".format(round(offse_sigma,3))   )
    saveParam.write("$\\alpha_{low}$   &"+"{}".format(round(slope_alphaLo,3))+"+$m_{X}$" +    "{}\\\\\n".format(round(offse_alphaLo,3)))
    saveParam.write("$\\alpha_{high}$  &"+"{}".format(round(slope_alphaHi,3))+"+$m_{X}$" +    "{}\\\\\n".format(round(offse_alphaHi,3)))
    saveParam.write("$\Delta m_{X}$    &"+"{}".format(round(slope_dmX,3))    +"+$m_{X}$" +    "{}\\\\\n".format(round(offse_dmX,3))    )
    saveParam.write("\\hline\n")
    saveParam.write("\\hline\n")
    saveParam.write("\\end{tabular}\n")
    saveParam.write("\\end{document}\n")
    saveParam.close()

    os.system("pdflatex results_period_{}_ptyy{}/paramResults/parametrisation.tex".format(period,ptyy))
    os.system("pdfcrop --margins '-0 -0 -0 -0' parametrisation.pdf parametrisation.pdf")
    os.system("mv parametrisation.pdf results_period_{}_ptyy{}/paramResults".format(period,ptyy))
    os.system("rm parametrisation.aux")
    os.system("rm parametrisation.log")

def plotParam_freeN():
    dNHi            = []
    dNHiErr         = []
    dNLo            = []
    dNLoErr         = []
    outputList = []
    for i in mp:
        fileName = "output_datacards/outputParam_FreeN_{}GeV.dat".format(i) 
        outputList.append(fileName)
    
    for f in outputList:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "cbNHi" in line:
                            dNHi.append(l[5].split("\\")[0])
                            dNHiErr.append(l[6].split("$")[0])
                        if "cbNLo" in line:
                            dNLo.append(l[5].split("\\")[0])
                            dNLoErr.append(l[6].split("$")[0])
    
    nbins = int((mp[len(mp)-1]+2.5-mp[0]+2.5)/5)
    hist_nHi       = ROOT.TH1F("nHi"      ,"nHi"      ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_nLo       = ROOT.TH1F("nLo"      ,"nLo"      ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    
    for i in range(len(dNHi)):
        iBin = hist_nHi.FindBin(mp[i])

        hist_nHi.SetBinContent(iBin,float(dNHi[i]))
        hist_nHi.SetBinError  (iBin,float(dNHiErr[i]))
        hist_nLo.SetBinContent(iBin,float(dNLo[i]))
        hist_nLo.SetBinError  (iBin,float(dNLoErr[i]))
    
    listOfHistos = [hist_nHi,hist_nLo]
    for histo in listOfHistos:
        if(histo==hist_nHi):
            histo.GetYaxis().SetTitle("N_{High}")
        #    histo.GetYaxis().SetRangeUser(5,15)
        if(histo==hist_nLo):
            histo.GetYaxis().SetTitle("N_{Low}")
        #    histo.GetYaxis().SetRangeUser(15,25)

        histo.SetStats(0)
        histo.SetMarkerSize(0.8)
        histo.SetMarkerStyle(20) 
        histo.SetLineWidth(2) 
        histo.SetLineColor(ROOT.kBlack) 
        histo.SetMarkerColor(ROOT.kBlack) 
        histo.SetTitle("")
        ROOT.gStyle.SetErrorX(0)
        ROOT.gStyle.SetOptFit(1111)
        histo.GetXaxis().SetTitle("m_{X} [GeV]")
    
#    saveParam = open("paramResults/parametrisationFreeN.txt", "w")

    c_red   = ROOT.TColor.GetColor('#b33c32')
    c_blue  = ROOT.TColor.GetColor('#3269b3')
    
    can = ROOT.TCanvas()
    leg=ROOT.TLegend(.68,.74,.88,.84)
    leg.SetBorderSize(0)
    hist_nHi.Fit("pol1")
    leg.AddEntry(hist_nHi,"Single Fit","P")
    leg.AddEntry(hist_nHi.GetFunction("pol1"),"Parametrisation","L")
    leg.Draw()
    hist_nHi.Draw("P")
    leg.Draw()
    hist_nHi.GetFunction("pol1").SetLineColor(c_blue)
    hist_nHi.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    offse_NHi = hist_nHi.GetFunction("pol1").SetParameter(0,10)
    slope_NHi = hist_nHi.GetFunction("pol1").SetParameter(1,0)
    can.Print("results_period_{}_ptyy{}/paramResults/nHi_Free.pdf".format(period,ptyy))
#    offse_NHi = hist_nHi.GetFunction("pol1").GetParameter(0)
#    slope_NHi = hist_nHi.GetFunction("pol1").GetParameter(1)
#    saveParam.write("paramNHi = {}*x+{}\n".format(slope_NHi,offse_NHi))

    hist_nLo.Fit("pol1")
    hist_nLo.Draw("P")
    leg.Draw()
    hist_nLo.GetFunction("pol1").SetLineColor(c_blue)
    hist_nLo.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    offse_NLo = hist_nLo.GetFunction("pol1").SetParameter(0,12)
    slope_NLo = hist_nLo.GetFunction("pol1").SetParameter(1,0)
    can.Print("results_period_{}_ptyy{}/paramResults/nLo_Free.pdf".format(period,ptyy))
#    offse_NLo = hist_nLo.GetFunction("pol1").GetParameter(0)
#    slope_NLo = hist_nLo.GetFunction("pol1").GetParameter(1)
#    saveParam.write("paramNLo = {}*x+{}\n".format(slope_NLo,offse_NLo))


def plotParam_ExtrafreeN():
    dNHi            = []
    dNHiErr         = []
    dNLo            = []
    dNLoErr         = []
    outputList = []
    for i in mp:
        fileName = "output_datacards_period_{}_ptyy{}/outputParam_ExtraFreeN_{}GeV.dat".format(period,ptyy,i) 
        outputList.append(fileName)
    
    for f in outputList:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "cbNHi" in line:
                            dNHi.append(l[5].split("\\")[0])
                            dNHiErr.append(l[6].split("$")[0])
                        if "cbNLo" in line:
                            dNLo.append(l[5].split("\\")[0])
                            dNLoErr.append(l[6].split("$")[0])
    
    nbins = int((mp[len(mp)-1]+2.5-mp[0]+2.5)/5)
    hist_nHi       = ROOT.TH1F("nHi"      ,"nHi"      ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_nLo       = ROOT.TH1F("nLo"      ,"nLo"      ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    
    avg_NHi = 0
    for i in dNHi:
        avg_NHi+=float(i)
    print("Average value for NHi = {}".format(avg_NHi/len(dNHi)))
    avg_NLo = 0
    for i in dNLo:
        avg_NLo+=float(i)
    print("Average value for NLo = {}".format(avg_NLo/len(dNLo)))
    
    for i in range(len(dNHi)):
        iBin = hist_nHi.FindBin(mp[i])

        hist_nHi.SetBinContent(iBin,float(dNHi[i]))
        hist_nHi.SetBinError  (iBin,float(dNHiErr[i]))
        hist_nLo.SetBinContent(iBin,float(dNLo[i]))
        hist_nLo.SetBinError  (iBin,float(dNLoErr[i]))
    
    listOfHistos = [hist_nHi,hist_nLo]
    for histo in listOfHistos:
        if(histo==hist_nHi):
            histo.GetYaxis().SetTitle("N_{High}")
        #    histo.GetYaxis().SetRangeUser(5,15)
        if(histo==hist_nLo):
            histo.GetYaxis().SetTitle("N_{Low}")
        #    histo.GetYaxis().SetRangeUser(15,25)

        histo.SetStats(0)
        histo.SetMarkerSize(0.8)
        histo.SetMarkerStyle(20) 
        histo.SetLineWidth(2) 
        histo.SetLineColor(ROOT.kBlack) 
        histo.SetMarkerColor(ROOT.kBlack) 
        histo.SetTitle("")
        ROOT.gStyle.SetErrorX(0)
        ROOT.gStyle.SetOptFit(1111)
        histo.GetXaxis().SetTitle("m_{X} [GeV]")
    
#    saveParam = open("paramResults/parametrisationFreeN.txt", "w")

    c_red   = ROOT.TColor.GetColor('#b33c32')
    c_blue  = ROOT.TColor.GetColor('#3269b3')
    
    can = ROOT.TCanvas()
    leg=ROOT.TLegend(.68,.74,.88,.84)
    leg.SetBorderSize(0)
    hist_nHi.Fit("pol1")
    leg.AddEntry(hist_nHi,"Single Fit","P")
    leg.AddEntry(hist_nHi.GetFunction("pol1"),"Parametrisation","L")
    leg.Draw()
    hist_nHi.Draw("P")
    leg.Draw()
    hist_nHi.GetFunction("pol1").SetLineColor(c_blue)
    hist_nHi.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    offse_NHi = hist_nHi.GetFunction("pol1").SetParameter(0,10)
    slope_NHi = hist_nHi.GetFunction("pol1").SetParameter(1,0)
    can.Print("results_period_{}_ptyy{}/paramResults/nHi_ExtraFree.pdf".format(period,ptyy))
#    offse_NHi = hist_nHi.GetFunction("pol1").GetParameter(0)
#    slope_NHi = hist_nHi.GetFunction("pol1").GetParameter(1)
#    saveParam.write("paramNHi = {}*x+{}\n".format(slope_NHi,offse_NHi))

    hist_nLo.Fit("pol1")
    hist_nLo.Draw("P")
    leg.Draw()
    hist_nLo.GetFunction("pol1").SetLineColor(c_blue)
    hist_nLo.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
    offse_NLo = hist_nLo.GetFunction("pol1").SetParameter(0,12)
    slope_NLo = hist_nLo.GetFunction("pol1").SetParameter(1,0)
    can.Print("results_period_{}_ptyy{}/paramResults/nLo_ExtraFree.pdf".format(period,ptyy))
#    offse_NLo = hist_nLo.GetFunction("pol1").GetParameter(0)
#    slope_NLo = hist_nLo.GetFunction("pol1").GetParameter(1)
#    saveParam.write("paramNLo = {}*x+{}\n".format(slope_NLo,offse_NLo))

def plotParam_syst():
    cbSigma_nom     = []
    cbSigmaErr_nom     = []
    cbSigma_down     = []
    cbSigmaErr_down     = []
    cbSigma_up     = []
    cbSigmaErr_up     = []
    dmX_nom         = []
    dmXErr_nom         = []
    dmX_up         = []
    dmXErr_up         = []
    dmX_down         = []
    dmXErr_down         = []
    outputList_nom = []
    outputList_res_up = []
    outputList_res_down = []
    outputList_sca_up = []
    outputList_sca_down = []
    for i in mp:
        fileName_nom = "output_datacards_period_{}_ptyy{}/nom/outputParam_{}GeV.dat".format(period,ptyy,i) 
        fileName_res_up = "output_datacards_period_{}_ptyy{}/resolUp/outputParam_{}GeV.dat".format(period,ptyy,i) 
        fileName_res_down = "output_datacards_period_{}_ptyy{}/resolDown/outputParam_{}GeV.dat".format(period,ptyy,i) 
        fileName_sca_up = "output_datacards_period_{}_ptyy{}/scaleUp/outputParam_{}GeV.dat".format(period,ptyy,i) 
        fileName_sca_down = "output_datacards_period_{}_ptyy{}/scaleDown/outputParam_{}GeV.dat".format(period,ptyy,i) 
        outputList_nom.append(fileName_nom)
        outputList_res_up.append(fileName_res_up)
        outputList_res_down.append(fileName_res_down)
        outputList_sca_up.append(fileName_sca_up)
        outputList_sca_down.append(fileName_sca_down)
    
    for f in outputList_nom:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "dX" in line:
                            dmX_nom.append(l[5].split("\\")[0])
                            dmXErr_nom.append(l[6].split("$")[0])
                        if "cbSigma" in line:
                            cbSigma_nom.append(l[5].split("\\")[0])
                            cbSigmaErr_nom.append(l[6].split("$")[0])
    
    for f in outputList_res_up:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "cbSigma" in line:
                            cbSigma_up.append(l[5].split("\\")[0])
                            cbSigmaErr_up.append(l[6].split("$")[0])

    for f in outputList_res_down:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "cbSigma" in line:
                            cbSigma_down.append(l[5].split("\\")[0])
                            cbSigmaErr_down.append(l[6].split("$")[0])

    for f in outputList_sca_up:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "dX" in line:
                            dmX_up.append(l[5].split("\\")[0])
                            dmXErr_up.append(l[6].split("$")[0])

    for f in outputList_sca_down:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "dX" in line:
                            dmX_down.append(l[4].split("\\")[0])
                            dmXErr_down.append(l[5].split("$")[0])
    
    nbins = int((mp[len(mp)-1]+2.5-mp[0]+2.5)/5)
##    print("number of bins {}".format(nbins))
##    print("mp[0]-2.5 {}".format(mp[0]-2.5))
##    print("mp[len(mp)-1]+2.5 {}".format(mp[len(mp)-1]+2.5))
    hist_cbSigma_nom   = ROOT.TH1F("cbSigma_nom"  ,"cbSigma_nom"  ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_cbSigma_down   = ROOT.TH1F("cbSigma_down"  ,"cbSigma_down"  ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_cbSigma_up   = ROOT.TH1F("cbSigma_up"  ,"cbSigma_up"  ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_dmX_nom       = ROOT.TH1F("dmX_nom"      ,"dmX_nom"      ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_dmX_up       = ROOT.TH1F("dmX_up"      ,"dmX_up"      ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_dmX_down       = ROOT.TH1F("dmX_down"      ,"dmX_down"      ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
#    
    for i in range(len(cbSigma_nom)):
        iBin = hist_cbSigma_nom.FindBin(mp[i])
#        print("mass points {}".format(mp))
#        print("mp[i] {}".format(mp[i]))
#        print("iBin {}".format(iBin))
#        print("bin centre {}".format(hist_cbSigma.GetBinCenter(iBin)))

        hist_cbSigma_nom    .SetBinContent(iBin,float(cbSigma_nom[i]))
        hist_cbSigma_nom    .SetBinError  (iBin,float(cbSigmaErr_nom[i]))
        hist_cbSigma_down    .SetBinContent(iBin,float(cbSigma_down[i]))
        hist_cbSigma_down    .SetBinError  (iBin,float(cbSigmaErr_down[i]))
        hist_cbSigma_up    .SetBinContent(iBin,float(cbSigma_up[i]))
        hist_cbSigma_up    .SetBinError  (iBin,float(cbSigmaErr_up[i]))
        hist_dmX_nom        .SetBinContent(iBin,float(dmX_nom[i]))
        hist_dmX_nom        .SetBinError  (iBin,float(dmXErr_nom[i]))
        hist_dmX_up        .SetBinContent(iBin,float(dmX_up[i]))
        hist_dmX_up        .SetBinError  (iBin,float(dmXErr_up[i]))
        hist_dmX_down        .SetBinContent(iBin,float(dmX_down[i]))
        hist_dmX_down        .SetBinError  (iBin,float(dmXErr_down[i]))
    
    listOfHistos = [hist_cbSigma_nom,hist_dmX_nom]

    for histo in listOfHistos:
        if(histo==hist_cbSigma_nom):
            histo.GetYaxis().SetTitle("#sigma_{CB} [GeV]")
            histo.GetYaxis().SetRangeUser(0,1.2)
        if(histo==hist_dmX_nom):
            histo.GetYaxis().SetTitle("#Delta m_{X}")
            histo.GetYaxis().SetRangeUser(-0.4,0.4)
#
        histo.SetStats(0)
        histo.SetMarkerSize(0.8)
        histo.SetMarkerStyle(20) 
        histo.SetLineWidth(2) 
        histo.SetLineColor(ROOT.kBlack) 
        histo.SetMarkerColor(ROOT.kBlack) 
        histo.SetTitle("")
        ROOT.gStyle.SetErrorX(0)
        ROOT.gStyle.SetOptFit(1111)
        histo.GetXaxis().SetTitle("m_{X} [GeV]")
    
#    os.system("mkdir results_period_{}_ptyy{}/paramResults".format(period,ptyy))       


    hist_cbSigma_up.SetLineColor(c_red)
    hist_cbSigma_down.SetLineColor(c_blue)
    hist_cbSigma_up.SetMarkerColor(c_red)
    hist_cbSigma_down.SetMarkerColor(c_blue)
    hist_cbSigma_up.  SetMarkerStyle(20)
    hist_cbSigma_down.SetMarkerStyle(20)
    hist_cbSigma_up.  SetMarkerSize(0.8)
    hist_cbSigma_down.SetMarkerSize(0.8)

    hist_dmX_up.SetLineColor(c_red)
    hist_dmX_down.SetLineColor(c_blue)
    hist_dmX_up.SetMarkerColor(c_red)
    hist_dmX_down.SetMarkerColor(c_blue)
    hist_dmX_up.  SetMarkerStyle(20)
    hist_dmX_down.SetMarkerStyle(20)
    hist_dmX_up.  SetMarkerSize(0.8)
    hist_dmX_down.SetMarkerSize(0.8)


    hist_cbSigma_nom_diff = hist_cbSigma_nom.Clone("hist_cbSigma_nom_diff")
    hist_cbSigma_up_diff = hist_cbSigma_nom.Clone("hist_cbSigma_up_diff")
    hist_cbSigma_down_diff = hist_cbSigma_nom.Clone("hist_cbSigma_down_diff")
    hist_cbSigma_down_diff.SetMarkerColor(c_blue)
    hist_cbSigma_up_diff.SetMarkerColor(c_red)

    hist_cbSigma_nom_diff .Add(hist_cbSigma_nom,-1)
    hist_cbSigma_up_diff  .Add(hist_cbSigma_up,-1)
    hist_cbSigma_down_diff.Add(hist_cbSigma_down,-1)   

    hist_dmX_nom_diff = hist_dmX_nom.Clone("hist_dmX_nom_diff")
    hist_dmX_up_diff = hist_dmX_nom.Clone("hist_dmX_up_diff")
    hist_dmX_down_diff = hist_dmX_nom.Clone("hist_dmX_down_diff")
    hist_dmX_down_diff.SetMarkerColor(c_blue)
    hist_dmX_up_diff.SetMarkerColor(c_red)

    hist_dmX_nom_diff .Add(hist_dmX_nom,-1)
    hist_dmX_up_diff  .Add(hist_dmX_up,-1)
    hist_dmX_down_diff.Add(hist_dmX_down,-1)   


    for b in range(hist_cbSigma_nom_diff.GetXaxis().GetNbins()):
        hist_cbSigma_nom_diff .GetYaxis().SetRangeUser(-0.05,0.05)
        hist_cbSigma_up_diff  .GetYaxis().SetRangeUser(-0.05,0.05)
        hist_cbSigma_down_diff.GetYaxis().SetRangeUser(-0.05,0.05)
        hist_cbSigma_nom_diff.SetBinError(b+1,0) 
        hist_cbSigma_up_diff  .SetBinError(b+1,0)
        hist_cbSigma_down_diff.SetBinError(b+1,0)
        hist_dmX_nom_diff .GetYaxis().SetRangeUser(-0.05,0.05)
        hist_dmX_up_diff  .GetYaxis().SetRangeUser(-0.05,0.05)
        hist_dmX_down_diff.GetYaxis().SetRangeUser(-0.05,0.05)
        hist_dmX_nom_diff.SetBinError(b+1,0) 
        hist_dmX_up_diff  .SetBinError(b+1,0)
        hist_dmX_down_diff.SetBinError(b+1,0)

    can = ROOT.TCanvas()
    padUp = ROOT.TPad("padUp", "padUp", 0, 0.29, 1, 1)
    padLo = ROOT.TPad("padLo", "padLo", 0, 0, 1, 0.33)
    padUp.SetBottomMargin(0.09)
    padUp.SetBorderMode(0)
    padLo.SetTopMargin(0.01)
    padLo.SetBottomMargin(0.25)
    padLo.SetBorderMode(0)
    padUp.Draw()
    padLo.Draw()
    leg=ROOT.TLegend(.68,.74,.88,.84)
    leg.SetBorderSize(0)
    padUp.cd()
    hist_cbSigma_nom.Draw()
    hist_cbSigma_nom.Draw()
    hist_cbSigma_up.Draw("same")
    hist_cbSigma_down.Draw("same")

#    leg.AddEntry(hist_cbSigma,"Single Fit","P")
    leg.Draw()
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")

    padLo.cd()
    hist_cbSigma_nom_diff .GetYaxis().SetTitle("nom-var")
    hist_cbSigma_nom_diff .GetYaxis().SetTitleSize(0.14)
    hist_cbSigma_nom_diff .GetYaxis().SetTitleOffset(0.14)
    hist_cbSigma_nom_diff .Draw("P")
    hist_cbSigma_up_diff  .Draw("sameP")
    hist_cbSigma_down_diff.Draw("sameP")

    can.Print("results_period_{}_ptyy{}/paramResults/sigma_syst.pdf".format(period,ptyy))

    padUp.cd()
    hist_dmX_nom.Draw()
    hist_dmX_nom.Draw()
    hist_dmX_up.Draw("same")
    hist_dmX_down.Draw("same")

#    leg.AddEntry(hist_cbSigma,"Single Fit","P")
    #leg.Draw()
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")

    padLo.cd()
    hist_dmX_nom_diff .GetYaxis().SetTitle("nom-var")
    hist_dmX_nom_diff .GetYaxis().SetTitleSize(0.14)
    hist_dmX_nom_diff .GetYaxis().SetTitleOffset(0.14)
    hist_dmX_nom_diff .GetYaxis().SetRangeUser(-.1,.1)
    hist_dmX_nom_diff .Draw("P")
    hist_dmX_up_diff  .Draw("sameP")
    hist_dmX_down_diff.Draw("sameP")

    can.Print("results_period_{}_ptyy{}/paramResults/dmX_syst.pdf".format(period,ptyy))

#
#
#    hist_dmX.Fit("pol1")
#    hist_dmX.Draw()
#    leg.Draw()
#    hist_dmX.GetFunction("pol1").SetLineColor(c_blue)
#    hist_dmX.GetFunction("pol1").SetLineStyle(2)
#    tag = ROOT.TLatex()
#    tag.SetNDC()
#    tag.SetTextSize(0.04)
#    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
#    tag.SetTextSize(0.03)
#    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
#    offse_dmX = hist_dmX.GetFunction("pol1").GetParameter(0)
#    slope_dmX = hist_dmX.GetFunction("pol1").GetParameter(1)
#    saveParam.write("paramdmX = {}*x+{}\n".format(slope_dmX,offse_dmX))
#    can.Print("results_period_{}_ptyy{}/paramResults/dmx.pdf".format(period,ptyy))
#
#    hist_nHi.Fit("pol1")
#    hist_nHi.Draw("P")
#    leg.Draw()
#    hist_nHi.GetFunction("pol1").SetLineColor(c_blue)
#    hist_nHi.GetFunction("pol1").SetLineStyle(2)
#    tag = ROOT.TLatex()
#    tag.SetNDC()
#    tag.SetTextSize(0.04)
#    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
#    tag.SetTextSize(0.03)
#    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
#    can.Print("results_period_{}_ptyy{}/paramResults/nHi.pdf".format(period,ptyy))
#
#    hist_nLo.Fit("pol1")
#    hist_nLo.Draw("P")
#    leg.Draw()
#    hist_nLo.GetFunction("pol1").SetLineColor(c_blue)
#    hist_nLo.GetFunction("pol1").SetLineStyle(2)
#    tag = ROOT.TLatex()
#    tag.SetNDC()
#    tag.SetTextSize(0.04)
#    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
#    tag.SetTextSize(0.03)
#    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
#    can.Print("results_period_{}_ptyy{}/paramResults/nLo.pdf".format(period,ptyy))
#
#    saveParam = open("results_period_{}_ptyy{}/paramResults/parametrisation.tex".format(period,ptyy), "w")
#    saveParam.write("\\documentclass{article}\n")
#    saveParam.write("\\pagestyle{empty}\n")
#    saveParam.write("\\begin{document}\n")
#    saveParam.write("\\begin{tabular}{l | l}\n")
#    saveParam.write("\\hline\n")
#    saveParam.write("\\hline\n")
#    saveParam.write("DSCB parameter  & Mass parametrisation\\\\\\hline\n")
#    saveParam.write("$\sigma_{CB}$     &"+"{}".format(round(slope_sigma,3))  +"+$m_{X}$" +    "{}\\\\\n".format(round(offse_sigma,3))   )
#    saveParam.write("$\\alpha_{low}$   &"+"{}".format(round(slope_alphaLo,3))+"+$m_{X}$" +    "{}\\\\\n".format(round(offse_alphaLo,3)))
#    saveParam.write("$\\alpha_{high}$  &"+"{}".format(round(slope_alphaHi,3))+"+$m_{X}$" +    "{}\\\\\n".format(round(offse_alphaHi,3)))
#    saveParam.write("$\Delta m_{X}$    &"+"{}".format(round(slope_dmX,3))    +"+$m_{X}$" +    "{}\\\\\n".format(round(offse_dmX,3))    )
#    saveParam.write("\\hline\n")
#    saveParam.write("\\hline\n")
#    saveParam.write("\\end{tabular}\n")
#    saveParam.write("\\end{document}\n")
#    saveParam.close()
#
#    os.system("pdflatex results_period_{}_ptyy{}/paramResults/parametrisation.tex".format(period,ptyy))
#    os.system("pdfcrop --margins '-0 -0 -0 -0' parametrisation.pdf parametrisation.pdf")
#    os.system("mv parametrisation.pdf results_period_{}_ptyy{}/paramResults".format(period,ptyy))
#    os.system("rm parametrisation.aux")
#    os.system("rm parametrisation.log")


def compParam():
    cbSigma_a     = []
    cbSigmaErr_a     = []
    outputList_a = []
    cbSigma_d     = []
    cbSigmaErr_d     = []
    outputList_d = []
    cbSigma_e     = []
    cbSigmaErr_e     = []
    outputList_e = []
    for i in mp:
        fileName = "output_datacards_period_a_ptyy{}/nom/outputParam_{}GeV.dat".format(ptyy,i) 
        outputList_a.append(fileName)
        fileName = "output_datacards_period_d_ptyy{}/nom/outputParam_{}GeV.dat".format(ptyy,i) 
        outputList_d.append(fileName)
        fileName = "output_datacards_period_e_ptyy{}/nom/outputParam_{}GeV.dat".format(ptyy,i) 
        outputList_e.append(fileName)
    
    for f in outputList_a:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "cbSigma" in line:
                            cbSigma_a.append(l[5].split("\\")[0])
                            cbSigmaErr_a.append(l[6].split("$")[0])
    for f in outputList_d:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "cbSigma" in line:
                            cbSigma_d.append(l[5].split("\\")[0])
                            cbSigmaErr_d.append(l[6].split("$")[0])
    for f in outputList_e:
                with open(f) as fl:
                    print("For mass point {}".format(f))
                    for line in fl:
                        l = line.split(" ")
                        if "cbSigma" in line:
                            cbSigma_e.append(l[5].split("\\")[0])
                            cbSigmaErr_e.append(l[6].split("$")[0])
    
    nbins = int((mp[len(mp)-1]+2.5-mp[0]+2.5)/5)
    hist_cbSigma_a   = ROOT.TH1F("cbSigma_a"  ,"cbSigma_a"  ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_cbSigma_d   = ROOT.TH1F("cbSigma_d"  ,"cbSigma_d"  ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    hist_cbSigma_e   = ROOT.TH1F("cbSigma_e"  ,"cbSigma_e"  ,nbins,mp[0]-2.5,mp[len(mp)-1]+2.5)
    
    for i in range(len(cbSigma_a)):
        iBin = hist_cbSigma_a.FindBin(mp[i])
        hist_cbSigma_a    .SetBinContent(iBin,float(cbSigma_a[i]))
        hist_cbSigma_a    .SetBinError  (iBin,float(cbSigmaErr_a[i]))
        hist_cbSigma_d    .SetBinContent(iBin,float(cbSigma_d[i]))
        hist_cbSigma_d    .SetBinError  (iBin,float(cbSigmaErr_d[i]))
        hist_cbSigma_e    .SetBinContent(iBin,float(cbSigma_e[i]))
        hist_cbSigma_e    .SetBinError  (iBin,float(cbSigmaErr_e[i]))
    
        hist_cbSigma_a.GetYaxis().SetTitle("#sigma_{CB} [GeV]")
#        hist_cbSigma_a.GetYaxis().SetRangeUser(0,1.8)
        hist_cbSigma_a.SetStats(0)
        hist_cbSigma_d.SetStats(0)
        hist_cbSigma_e.SetStats(0)
        hist_cbSigma_a.SetMarkerSize(0.8)
        hist_cbSigma_a.SetMarkerStyle(20) 
        hist_cbSigma_a.SetLineWidth(2) 
        hist_cbSigma_a.SetLineColor(c_green) 
        hist_cbSigma_a.SetMarkerColor(c_green) 
        hist_cbSigma_a.SetTitle("")
        ROOT.gStyle.SetErrorX(0)
        ROOT.gStyle.SetOptFit(1111)
        hist_cbSigma_a.GetXaxis().SetTitle("m_{X} [GeV]")

        hist_cbSigma_d.SetMarkerSize(0.8)
        hist_cbSigma_d.SetMarkerStyle(20) 
        hist_cbSigma_d.SetLineWidth(2) 
        hist_cbSigma_d.SetLineColor(c_red) 
        hist_cbSigma_d.SetMarkerColor(c_red) 

        hist_cbSigma_e.SetMarkerSize(0.8)
        hist_cbSigma_e.SetMarkerStyle(20) 
        hist_cbSigma_e.SetLineWidth(2) 
        hist_cbSigma_e.SetLineColor(c_blue) 
        hist_cbSigma_e.SetMarkerColor(c_blue) 


    
    can = ROOT.TCanvas()
    leg=ROOT.TLegend(.64,.18,.88,.28)
    leg.SetBorderSize(0)
    f_a = ROOT.TF1("f_a","pol1",7.5,72.5);
    hist_cbSigma_a.Fit(f_a)
    hist_cbSigma_d.Fit("pol1")
    hist_cbSigma_e.Fit("pol1")
    hist_cbSigma_a.Draw()
    leg.AddEntry(hist_cbSigma_a,"Single fits - MC16a","P")
    leg.AddEntry(hist_cbSigma_d,"Single fits - MC16d","P")
    leg.AddEntry(hist_cbSigma_e,"Single fits - MC16e","P")
#    leg.AddEntry(hist_cbSigma_a.GetFunction("pol1"),"Parametrisation","L")

    hist_cbSigma_d.Draw("same")
    hist_cbSigma_e.Draw("same")
    
    leg.Draw()

    hist_cbSigma_a.GetFunction("f_a").SetLineColor(c_green)
    hist_cbSigma_a.GetFunction("f_a").SetLineStyle(2)
    hist_cbSigma_d.GetFunction("pol1").SetLineColor(c_red)
    hist_cbSigma_d.GetFunction("pol1").SetLineStyle(2)
    hist_cbSigma_e.GetFunction("pol1").SetLineColor(c_blue)
    hist_cbSigma_e.GetFunction("pol1").SetLineStyle(2)
    tag = ROOT.TLatex()
    tag.SetNDC()
    tag.SetTextSize(0.04)
    tag.DrawLatex(0.15, 0.82, "#it{ATLAS} #bf{Internal}")
    tag.SetTextSize(0.03)
    tag.DrawLatex(0.15, 0.77, "#bf{Scalar NW}")
#    offse_sigma = hist_cbSigma_a.GetFunction("pol1").GetParameter(0)
#    slope_sigma = hist_cbSigma_a.GetFunction("pol1").GetParameter(1)
    can.Print("sigma_compPer.pdf")





