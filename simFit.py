import ROOT
from ROOT import RooFit

useParam=True
ROOT.gROOT.ProcessLine(".L HggTwoSidedCBPdf.cxx")
#myy = [10,15]
myy = [10,15,20,25,30,35,40,45,50,55,60]

parametrisation = open("parametrisation.txt")
lin = parametrisation.readlines()
sigPar = lin[0].split("=")[1]
aHiPar = lin[1].split("=")[1]
aLoPar = lin[2].split("=")[1]
dmXPar = lin[3].split("=")[1]

paramSigma = ROOT.TF1("paramSigma","{}".format(sigPar),5, 65) 
paramAlphaHi = ROOT.TF1("paramAlphaHi","{}".format(aHiPar),5, 65) 
paramAlphaLo = ROOT.TF1("paramAlphaLo","{}".format(aLoPar),5, 65) 
paramdmX = ROOT.TF1("paramdmX","{}".format(dmXPar),5, 65) 

initParamVal_Sigma = []
initParamVal_AlphaHi = []
initParamVal_AlphaLo = []
initParamVal_dmX = []
for index in myy:
    initParamVal_Sigma.append(paramSigma.Eval(index))
    initParamVal_AlphaHi.append(paramAlphaHi.Eval(index))
    initParamVal_AlphaLo.append(paramAlphaLo.Eval(index))
    initParamVal_dmX.append(paramdmX.Eval(index))


mgg = ROOT.RooRealVar("mgg", "mgg", 5, 65, "GeV") 
sgSample = ROOT.RooCategory("sgSample","sgSample") 
weights = []
inputFiles = []
trees = []
dataSets = []
i=0
for index in myy:
    sgSample.defineType("m{}GeV".format(index))
    weights.append( ROOT.RooRealVar( "weight", "weight10", 0.0, 1.0 )) 
    inputFiles.append(ROOT.TFile("mc16_13TeV_MG{}.root".format(index)))
    trees.append(inputFiles[i].Get("tree"))
    dataSets.append(ROOT.RooDataSet("ds{}".format(index),"ds{}".format(index),trees[i], ROOT.RooArgSet(mgg,weights[i]),"","weight"))
    i+=1

#Now you need to build the combined dataset. RooFit in python has some issues and if you want to import more than 7 categories it crashes... why? who knows... So you need to make a map, but since the map is a c object you need to generate the dictionary
#combData = ROOT.RooDataSet("combData","combined data",ROOT.RooArgSet(mgg),RooFit.Index(sgSample),RooFit.Import("m10GeV",dataSets[0]),RooFit.Import("m15GeV",dataSets[1]),RooFit.Import("m20GeV",dataSets[2]),RooFit.Import("m25GeV",dataSets[3]),RooFit.Import("m30GeV",dataSets[4]),RooFit.Import("m35GeV",dataSets[5]),RooFit.Import("m40GeV",dataSets[6])) 
ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooDataSet*>", "map;string;RooDataSet.h")
ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooDataSet*>", "map;string;RooDataSet.h")        
ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooDataSet*>::iterator, bool>", "map;string;RooDataSet.h")
                 
sgMassMap = ROOT.std.map('string, RooDataSet*')()
sgMassMap.keepalive =list()

i=0
for index in myy:
    sgMassMap.keepalive.append(dataSets[i])
    sgMassMap.insert(sgMassMap.cbegin(), ROOT.std.pair("const string,RooDataSet*")("m{}GeV".format(index),dataSets[i]))
    i=i+1

combData = ROOT.RooDataSet("combData","combined data",ROOT.RooArgSet(mgg),RooFit.Index(sgSample), RooFit.Import(sgMassMap) )

dmX = []
cbSigma = []
cbAlphaHi = []
cbAlphaLo = []
cbNHi = []
cbNLo = []
m0 = []
pdfSg = []
simPDF = ROOT.RooSimultaneous("simPdf","simultaneous pdf",sgSample) ;

i=0
for index in myy:
    dmX      .append(ROOT.RooRealVar   ("dmX{}".format(index)       ,"dmX{}".format(index)       ,0.25   ,-3 ,3))
    cbSigma  .append(ROOT.RooRealVar   ("cbSigma{}  ".format(index),"cbSigma{}  ".format(index),0.25   ,0   ,10))
    cbAlphaHi.append(ROOT.RooRealVar   ("cbAlphaHi{}".format(index),"cbAlphaHi{}".format(index),0.9    ,0   ,10))
    cbAlphaLo.append(ROOT.RooRealVar   ("cbAlphaLo{}".format(index),"cbAlphaLo{}".format(index),2.5    ,0   ,10))
    cbNHi    .append(ROOT.RooRealVar   ("cbNHi{}    ".format(index),"cbNHi{}    ".format(index),10     ,0   ,100))
    cbNLo    .append(ROOT.RooRealVar   ("cbNLo{}    ".format(index),"cbNLo{}    ".format(index),20     ,0   ,100))
    cbNLo[i].setConstant()
    cbNHi[i].setConstant()
    m0      .append(ROOT.RooFormulaVar("m0{}".format(index)       ,"{}+dmX{}".format(int(index),index)    ,ROOT.RooArgList(dmX[i])))

    if(useParam==True):
        print("Using initial values from parametrisation")
        dmX[i].setVal(initParamVal_dmX[i])
        cbSigma[i].setVal(initParamVal_Sigma[i])
        cbAlphaLo[i].setVal(initParamVal_AlphaLo[i])
        cbAlphaHi[i].setVal(initParamVal_AlphaHi[i])
    pdfSg.append(ROOT.HggTwoSidedCBPdf("dscb{}".format(index), "dscb{}".format(index), mgg, m0[i], cbSigma[i],cbAlphaLo[i],cbNLo[i],cbAlphaHi[i],cbNHi[i]))

    simPDF.addPdf(pdfSg[i],"m{}GeV".format(index))
    i=i+1


fitResult = simPDF.fitTo(combData)
params = simPDF.getParameters(combData)

params.printLatex(RooFit.OutputFile("outputParam_simFit.dat".format(index)))

c = ROOT.TCanvas("can", "", 30, 60, 1000, 1000)
padUp = ROOT.TPad("padUp", "padUp", 0, 0.29, 1, 1)
padLo = ROOT.TPad("padLo", "padLo", 0, 0, 1, 0.33)
padUp.SetBottomMargin(0.09)
padUp.SetBorderMode(0)
padLo.SetTopMargin(0.01)
padLo.SetBottomMargin(0.25)
padLo.SetBorderMode(0)
padUp.Draw()
padLo.Draw()
for index in myy:
    mggMin = float("{}".format(index-index*.25))
    mggMax = float("{}".format(index+index*.25))
    padUp.cd()
    frame1 = mgg.frame(RooFit.Range(mggMin,mggMax))
    #frame1 = mgg.frame(RooFit.Bins(50), RooFit.Title("myy"), RooFit.Range(index-5,index+5))
    combData.plotOn(frame1, RooFit.Cut("sgSample==sgSample::m{}GeV".format(index)))

    simPDF.plotOn(frame1, RooFit.Slice(sgSample, "m{}GeV".format(index)), RooFit.ProjWData(ROOT.RooArgSet(sgSample), combData))
    frame1.Draw()

    padLo.cd()
    hres = frame1.residHist()
    frRes = mgg.frame(RooFit.Title("Residual Distribution"),RooFit.Range(mggMin,mggMax))
    frRes.addPlotable(hres,"P")
    frRes.SetTitle("")
    frRes.GetYaxis().SetTitle("residual")
    frRes.GetXaxis().SetTitleSize(0.1)
    frRes.GetYaxis().SetTitleSize(0.15)
    frRes.GetYaxis().SetTitleOffset(.15)
    frRes.GetYaxis().SetLabelSize(0.1)
    frRes.GetXaxis().SetLabelSize(0.1)
    frRes.Draw()
    

    c.Print("simFit_{}GeV.eps".format(index))





